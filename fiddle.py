from shapely.geometry import Point, LineString, Polygon, MultiPolygon
from shapely.geometry.polygon import orient
from shapely.ops import cascaded_union

import numpy as np
import pandas as pd
from pandas.util.testing import assert_frame_equal
import matplotlib.pyplot as plt
from enum import Enum
import subprocess
import os, re
import logging
import hashlib

logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)


def plotter(items, filename=None):
    fig, axs = plt.subplots()
    fig.set_size_inches(5., 5.)
    axs.axis('equal')
    plt.title(filename)
    for idx, item in enumerate(items):
        if item.geometryType() is 'LineString':
            axs.plot(*item.coords.xy, color='rgb'[idx % 3])
        elif item.geometryType() is 'MultiLineString':
            for linestring in item:
                axs.plot(*linestring.coords.xy, color='rgb'[idx % 3])
        elif item.geometryType() is 'MultiPolygon':
            for poly in item:
                xs, ys = poly.exterior.xy
                axs.fill(xs, ys, alpha=0.5, fc='rgb'[idx % 3], ec='none')
        elif hasattr(item, 'interiors') and len(item.interiors) > 0:
            axs.plot(*item.exterior.xy, color='k')
            for interior in item.interiors:
                axs.plot(*interior.xy, color='r')
        else:
            xs, ys = item.exterior.xy
            axs.fill(xs, ys, alpha=0.5, fc='rgb'[idx % 3], ec='none')
    if filename is None:
        plt.show() #if interactive
    else:
        plt.savefig(filename)

def poly_to_points(poly, ccw=True):
    return np.array(
        orient(poly, 1 if ccw else -1).exterior.coords.xy
    ).T[:-1]

def md5(contents):
    return hashlib.md5(contents.encode('utf-8')).hexdigest()


def purge(dir, pattern):
    for f in os.listdir(dir):
        if re.search(pattern, f):
            os.remove(os.path.join(dir, f))

points_strange = np.array([[0,0],[1,0],[0.4,0.2],[1,1],[0,1],[0.2,0.4]])
poly_strange = Polygon(points_strange)
poly_strange

points_square = np.array([[0,0],[1,0],[1,1],[0,1]])
poly_square = Polygon(points_square)
poly_square

points_diamond = np.array([[0,0],[-1,1],[0,2],[1,1]])
poly_diamond = Polygon(points_diamond)
poly_diamond

points_tiny = np.array([[0,0],[-1,1],[0,2],[1,1]]) * 0.2
poly_tiny = Polygon(points_tiny)
poly_tiny

points_rectangle = np.array([[0,0],[4,0],[4,1],[0,1]])
poly_rectangle = Polygon(points_rectangle)
poly_rectangle
# plotter([poly_rectangle, poly_strange], 'junk.png')
# plotter([poly_rectangle, poly_strange])

# for i in MultiPolygon([poly_rectangle, poly_diamond]):
#     pass
#
# i

poly_multi_shrunk_factor = 0.75
poly_multi = Polygon(
        np.array([[0,0],[10,0],[10,10],[0,10]]) * poly_multi_shrunk_factor,
        [
            np.array([[2,2],[4,2],[4,4],[2,4]]) * poly_multi_shrunk_factor,
            np.array([[6,6],[8,6],[8,8],[6,8]]) * poly_multi_shrunk_factor,
        ]
    )
poly_multi
poly_lobes = Polygon(
        np.array([[0,0],[10,0],[10,10],[0,10]]) * poly_multi_shrunk_factor,
        [
            np.array([[2,2],[4,2],[5,5],[2,4]]) * poly_multi_shrunk_factor,
            np.array([[5,5],[8,6],[8,8],[6,8]]) * poly_multi_shrunk_factor,
        ]
    )
poly_lobes


# plotter([poly_diamond, poly_multi, poly_rectangle, poly_square, poly_tiny, poly_lobes])


def idx_offset(idx, pts, delta=1):
    return (idx + delta) % pts.shape[0]

def d_abp(a, b, p):
    """
    Known in the literature as D-Function
    Given a vector a->b, determine side of point p
    +1 -> left, -1 -> right, 0=colinear
    """
    value = (a[0] - b[0]) * (a[1] - p[1]) - (a[1] - b[1]) * (a[0] - p[0])
    return value
    # if value > 0:
    #     return D_Side.left
    # if value < 0:
    #     return D_Side.right
    # return D_Side.colinear

assert d_abp(np.array([0,0]), np.array([1,0]), np.array([1,0])) == 0
assert d_abp(np.array([0,0]), np.array([1,0]), np.array([500,0])) == 0
assert d_abp(np.array([0,0]), np.array([1,0]), np.array([-500,0])) == 0
assert d_abp(np.array([0,0]), np.array([1,0]), np.array([-500,1])) > 0
assert d_abp(np.array([0,0]), np.array([1,0]), np.array([-500,-1])) < 0
assert d_abp(np.array([0,0]), np.array([1,0]), np.array([500,1])) > 0
assert d_abp(np.array([0,0]), np.array([1,0]), np.array([500,-1])) < 0
assert d_abp(np.array([0,0]), np.array([1,1]), np.array([0,1])) > 0
assert d_abp(np.array([0,0]), np.array([1,1]), np.array([1,0])) < 0
assert d_abp(np.array([0,0]), np.array([-1,-1]), np.array([1,0])) > 0
assert d_abp(np.array([0,0]), np.array([-1,-1]), np.array([0,1])) < 0

def on_segment(u, v, a):
    """Checks if a on on segment defined by u and v"""
    # Duva = d_adp(u,v,a)
    return np.allclose(LineString([u,v]).distance(Point(a)), 0.0)

assert on_segment(np.array([0,0]), np.array([1,0]), np.array([0,0]))
assert on_segment(np.array([0,0]), np.array([1,0]), np.array([1,0]))
assert on_segment(np.array([0,0]), np.array([1,0]), np.array([0.4,0]))
u = np.array([2433,3223])
v = np.array([394,-428])
assert on_segment(u, v, (u-v) * 0.5 + v)
assert not on_segment(u, v, (u-v) * 1.5 + v)

def segment_collide(line_string, polygon):
    """Check if the segment collides with the poly at all. Doesn't count if
    the line is along an edge"""
    return line_string.intersects(polygon) and not line_string.touches(polygon)

assert not segment_collide(LineString([np.array([1,1]), np.array([0,1])]), poly_square)
assert not segment_collide(LineString([np.array([1,7]), np.array([0,7])]), poly_square)
assert segment_collide(LineString([np.array([1,1]), np.array([0.5,0.5])]), poly_square)

assert not segment_collide(LineString([np.array([0.6,0.2]), np.array([1,0.2])]), poly_strange)
assert not segment_collide(LineString([np.array([1,0]), np.array([1,1])]), poly_strange)
assert segment_collide(LineString([np.array([0.2,0.2]), np.array([1,0.2])]), poly_strange)
assert segment_collide(LineString([np.array([0.8,-0.2]), np.array([0.8,0.2])]), poly_strange)


def segment_collide_point(line_string, polygon, inside=False):
    # pull the base up, just enough to prevent collisions there

    if not segment_collide(line_string, polygon) or \
        not line_string.crosses(polygon.boundary):
        return None
    # https://stackoverflow.com/a/28036167/2601448
    # BUffer for floating point errors
    res = line_string.intersection(polygon.buffer(-1e-12))
    # res = line_string.intersection(polygon.buffer((-polygon.area ** 0.5) / 1e7))
    if res.geometryType() is 'LineString':
        item = res
        if not inside and item.length < 1e-9:
            # clipping just too close
            logging.info(f"clipping just too close {item.length}")
            return None
    elif res.geometryType() is 'MultiLineString':
        assert len(res) > 0
        item = res[0]
    elif res.geometryType() is 'GeometryCollection':
        # logging.info(np.array(line_string.coords.xy).T)
        # logging.info(np.array(polygon.exterior.coords.xy).T)
        line_strings = [r for r in res if r.geometryType() is 'LineString']
        if len(line_strings) < 1:
            return None
        item = line_strings[0]
    elif res.geometryType() is 'Point':
        # plotter([polygon, line_string, res.buffer(0.1)])
        return np.array(res.coords.xy).T[0]
        # raise Exception(f"Invalid type supplied - {res.geometryType()}")
    else:
        raise Exception(f"Invalid type supplied - {res.geometryType()}")
    detected_point = np.array(item.coords.xy).T[1 if inside else 0]
    # logging.info(f"picked line {item}")
    # plotter([polygon, line_string, item])
    # if np.allclose(detected_point, ):
    #     return None
    return detected_point


assert segment_collide_point(LineString([(1.,1.), (5.6,5.0)]), poly_square) is None
assert np.allclose(segment_collide_point(LineString([(0.5, 50),(0.5,0.5)]), poly_square), np.array([0.5, 1]))
assert segment_collide_point(LineString([(0.,0.), (0.6,0)]), poly_strange) is None
assert segment_collide_point(LineString([(0.6,-0.2), (0.6,1.5)]), poly_strange) is not None
assert np.allclose(segment_collide_point(LineString([(0.6,-0.2), (0.6,1.5)]), poly_strange).round(2), np.array([0.6, 0.0]))

def segment_collide_distance(line_string, polygon, inside=False, startpoint=None, idx=0):
    # logging.info(f"poly Touch line {polygon.touches(linestring)}")
    # logging.info(f"line Touch poly {linestring.touches(polygon)}")
    point = segment_collide_point(line_string, polygon, inside)
    if point is None:
        return point
    line_string_new = LineString([np.array(line_string.xy).T[0], point])

    if inside and idx > 0:
        # logging.info(f"Checking for start point intersection @ {line_string.distance(Point(startpoint))}")
        if line_string_new.distance(Point(startpoint)) < 1e-9:
            # if not inside and polygon.touches(linestring):
            #     # touching at base
            #     logging.info("")
            #     return None
            line_string_final = LineString([np.array(line_string_new.xy).T[0], startpoint])
            return line_string_final.length
    return line_string_new.length


assert np.allclose(segment_collide_distance(LineString([(-5,0.5), (0.5,0.5)]), poly_square), 5.)
assert np.allclose(segment_collide_distance(LineString([(-1,2), (0.5,0.5)]), poly_square), 2 ** 0.5)
assert np.allclose(segment_collide_distance(LineString([(0.5,0.5), (1.5,0.5)]), poly_square, inside=True), 0.5)
assert np.allclose(segment_collide_distance(LineString([(0.5,0.5), (1.5,0.5)]), poly_square, inside=True, startpoint=[0.75,0.5], idx=50), 0.25)
assert np.allclose(segment_collide_distance(LineString([(0.6,-0.2), (0.6,1.5)]), poly_strange), 0.2)


def min_clean(lst):
    distances_clean = [distance for distance in lst if distance is not None]
    if len(distances_clean) < 1:
        return None
    return f"{min(distances_clean):4.3f}"

def slide_distance(poly_stationary, pts_orbiting, offset_orbiting, vector, inside=False, startpoint=None, idx=None):
    """maximum that can be slid. None means no limit"""
    poly_orbiting_offset = Polygon(pts_orbiting + offset_orbiting)
    # poly_orbiting = Polygon(pts_orbiting + offset_orbiting)
    distances = []
    markers = []
    orbiting_linestings = [LineString([pt_set, pt_set+vector]) for pt_set in pts_orbiting + offset_orbiting]
    for linestring in orbiting_linestings:
        distance = segment_collide_distance(linestring, poly_stationary, inside=inside, startpoint=startpoint, idx=idx)
        logging.info(f"{linestring} -> {distance}")
        if distance is not None:
            logging.info(f"caught {distance}")
            markers.append(linestring.interpolate(distance).buffer(0.04))
        if distance is not None and np.allclose(distance, 0.):
            logging.info(f"Strike from orbit to stationary {distance:4.3f}")
            # logging.info(pt_set)
            # logging.info(pt_set+vector)
            pass
        distances.append(distance)
    # plotter([poly_stationary, poly_orbiting_offset] + orbiting_linestings + markers, f"orbit_lines_{min_clean(distances)}.png")

    # project backwards
    poly_oriented = orient(poly_stationary, 1)
    # get interior and exterior
    pts_sets = [np.array(poly_oriented.exterior.coords.xy).T[:-1]] + [np.array(interior.coords.xy).T[:-1] for interior in poly_oriented.interiors]
    stationary_linestrings = []
    markers = []
    distances_stationary = []
    for pts_stationary in pts_sets:
        for pt_set in pts_stationary:
            stationary_linestrings.append(LineString([pt_set, pt_set - vector]))

    for linestring in stationary_linestrings:
        distance = segment_collide_distance(linestring, poly_orbiting_offset, inside=False)
        # distance = segment_collide_distance(LineString([pt_set, pt_set + vector]), poly_orbiting_offset)
        # logging.info('Back')
        # logging.info(pt_set)
        # logging.info(pt_set-vector)
        if distance is not None:
            logging.info(f"caught2 {distance}")
            # logging.info(pt_set)
            logging.info(linestring.wkt)
            logging.info(poly_orbiting_offset.wkt)
            logging.info(f"caught2 {distance}")
            markers.append(linestring.interpolate(distance).buffer(0.05))
        if distance is not None and np.allclose(distance, 0.):
            logging.info(f"Strike from stationary to orbit {distance:4.3f}")
            # logging.info(f'Back - success {distance}')
            # logging.info(pt_set)
            # logging.info(pt_set - vector)
            pass
        distances_stationary.append(distance)

    # plotter([poly_stationary, poly_orbiting_offset] + stationary_linestrings + markers, f"stationary_lines_{min_clean(distances_stationary)}.png")

    distances_clean = [distance for distance in (distances + distances_stationary) if distance is not None]
    # logging.info(distances)
    logging.info(distances_clean)
    if len(distances_clean) < 1:
        return None
    return min(distances_clean)



def no_collisions(poly_stationary, pts_orbiting, offset_orbiting, vector):
    # Ensure no obvious hits
    poly_orbiting_offset = Polygon(pts_orbiting + offset_orbiting)
    linestrings = [LineString([pt_set, pt_set+vector]) for pt_set in pts_orbiting + offset_orbiting]
    # plotter([poly_stationary, poly_orbiting_offset] + linestrings)
    for linestring in linestrings:
        if segment_collide(linestring, poly_stationary):
            return False
    return True

def generate_touching_pts(poly_stationary, pts_orbiting, offset_orbiting):
    touching = []
    poly_oriented = orient(poly_stationary, 1)
    pts_sets = [np.array(poly_oriented.exterior.coords.xy).T[:-1]] + [np.array(interior.coords.xy).T[:-1] for interior in poly_oriented.interiors]

    for idx_ring, pts_stationary in enumerate(pts_sets):
        for idx_stationary in range(pts_stationary.shape[0]):
            idx_stationary_next = idx_offset(idx_stationary, pts_stationary, delta=1)
            for idx_orbiting in range(pts_orbiting.shape[0]):
                idx_orbiting_next = idx_offset(idx_orbiting, pts_orbiting, delta=1)
                if np.allclose(
                    pts_stationary[idx_stationary],
                    pts_orbiting[idx_orbiting] + offset_orbiting
                ):
                    # if points match
                    touching.append({'type': 0, 'stationary': idx_stationary, 'stationary_idx': idx_ring, 'orbiting': idx_orbiting})
                elif on_segment(
                    pts_stationary[idx_stationary],
                    pts_stationary[idx_stationary_next],
                    pts_orbiting[idx_orbiting] + offset_orbiting
                ):
                    # if touching point is on stationary segment
                    touching.append({'type': 1, 'stationary': idx_stationary_next, 'stationary_idx': idx_ring, 'orbiting': idx_orbiting})
                elif on_segment(
                    pts_orbiting[idx_orbiting] + offset_orbiting,
                    pts_orbiting[idx_orbiting_next] + offset_orbiting,
                    pts_stationary[idx_stationary],
                ):
                    # if touching point is on orbiting segment
                    touching.append({'type': 2, 'stationary': idx_stationary, 'stationary_idx': idx_ring, 'orbiting': idx_orbiting_next})

    assert len(touching) > 0, "No touching points found"
    df = pd.DataFrame(touching)
    return df.sort_values(
        'type'
    )[
        ~df.sort_values(
            'type'
        ).drop(
            ['type'],
            axis=1
        ).duplicated(
            keep='first'
        )
    ].reset_index(
        drop=True
    )



touching_test = generate_touching_pts(
    Polygon(
        np.array([
            [0., 0.],
            [1., 0.],
            [0.4, 0.2],
            [1., 1.],
            [0., 1.],
            [0.2, 0.4],
        ])
    ),
    np.array([
        [0., 0.],
        [1., 1.],
        [0., 2.],
        [-1., 1.],
    ]),
    np.array(
        [1.57142857, -0.57142857]
    )
)

assert_frame_equal(
        touching_test,
        pd.DataFrame({
            "orbiting": pd.Series([3, 0], dtype="int64"),
            "stationary": pd.Series([3, 1], dtype="int64"),
            "stationary_idx": pd.Series([0, 0], dtype="int64"),
            "type": pd.Series([1, 2], dtype="int64"),
        })
    )



def pt_next_prev(pts, idx):
    """Return the point, the next, and the previous, handing wrap arounds"""
    idx_next = idx_offset(idx, pts, delta=1)
    idx_prev = idx_offset(idx, pts, delta=-1)

    pt = pts[idx]
    pt_next = pts[idx_next]
    pt_prev = pts[idx_prev]

    return pt, pt_next, pt_prev


def vectors_from_touches(touching, poly_stationary, pts_orbiting, inside=False):
    # assumes points include offsets
    assert not touching.empty, "Need to have some points of contact"

    poly_oriented = orient(poly_stationary, 1)
    pts_sets = [np.array(poly_oriented.exterior.coords.xy).T[:-1]] + [np.array(interior.coords.xy).T[:-1] for interior in poly_oriented.interiors]


    # if inside:
    #     # swap them for selection when inside
    #     pts_stationary, pts_orbiting = pts_orbiting, pts_stationary
    vectors = []
    for _, touch in touching.iterrows():

        idx_stationary = touch['stationary']
        idx_ring = touch['stationary_idx']
        idx_orbiting = touch['orbiting']
        pts_stationary = pts_sets[idx_ring]
        # pts_stationary = pts_sets[idx_ring][::-1] if idx_ring > 0 else pts_sets[idx_ring]

        pt_stationary, pt_stationary_next, pt_stationary_prev = \
            pt_next_prev(pts_stationary, idx_stationary)
        pt_orbiting, pt_orbiting_next, pt_orbiting_prev = \
            pt_next_prev(pts_orbiting, idx_orbiting)

        # if points match
        if touch['type'] == 0:
            logging.info("Type 1 - points line up")
            # from kendall 152
            # NOTE: Kendall has a typo with leveraging the DFuncs
            pt_a = pt_stationary_prev
            pt_b = pt_stationary
            pt_c = pt_stationary_next
            pt_d = pt_orbiting_prev
            pt_e = pt_orbiting
            pt_f = pt_orbiting_next

        # if touching point is on stationary segment
        elif touch['type'] == 1:
            pt_a = pt_stationary_prev
            pt_b = pt_orbiting
            pt_c = pt_stationary
            pt_d = pt_orbiting_prev
            pt_e = pt_orbiting
            pt_f = pt_orbiting_next

        # if touching point is on orbiting segment
        elif touch['type'] == 2:
            pt_a = pt_stationary_prev
            pt_b = pt_stationary
            pt_c = pt_stationary_next
            pt_d = pt_orbiting_prev
            pt_e = pt_stationary
            pt_f = pt_orbiting

        else:
            raise Exception("Invalid Type {touch['type']}")

        # logging.info(f"----")
        # if idx_ring > 0:
        #     logging.info("Hit an interior!!!!")
        # logging.info(f"A: {pt_a.round(1)}")
        # logging.info(f"B: {pt_b.round(1)}")
        # logging.info(f"C: {pt_c.round(1)}")
        # logging.info(f"D: {pt_d.round(1)}")
        # logging.info(f"E: {pt_e.round(1)}")
        # logging.info(f"F: {pt_f.round(1)}")
        # logging.info(f"----")
        d_result = d_abp(pt_b, pt_c, pt_f)
        if np.allclose(d_result, 0) or d_result > 0:
            if np.allclose(d_result, 0):
                # logging.info('Found Colinear')
                pass
            else:
                # logging.info('Found Left')
                pass
            # left or colinear
            if not inside:
                # logging.info(f"sliding edge is EF")
                # sliding edge is EF
                # sliding point is B
                vectors.append({
                    'pt_sliding': pt_b,
                    'delta': pt_e - pt_f,
                })
            else:
                # logging.info(f"sliding edge is BC")
                # sliding edge is BC
                # sliding point is E
                vectors.append({
                    'pt_sliding': pt_e,
                    'delta': pt_c - pt_b,
                })
                if idx_ring > 0:
                    # Backwards check on interior
                    # logging.info("Backwards check on interior")
                    vectors.append({
                        'pt_sliding': pt_b,
                        'delta': pt_e - pt_d,
                    })
                    pass

        elif d_result < 0:
            # logging.info('Found Right')
            # right
            if not inside:
                # logging.info(f"sliding edge is BC")
                # sliding edge is BC
                # sliding point is E
                vectors.append({
                    'pt_sliding': pt_e,
                    'delta': pt_c - pt_b,
                })
            else:
                # logging.info(f"sliding edge is EF")
                # sliding edge is EF
                # sliding point is B
                vectors.append({
                    'pt_sliding': pt_b,
                    'delta': pt_e - pt_f,
                })

    return vectors


def inside_startpoint(poly_stationary, pts_orbiting, poly_stationary_must_contain):
    # NOTE: This needs to handle multi-polygons
    # It can do so by treating each linear ring as a potential placement
    # of course, it needs to elminate with multiple passes
    # Can take each nfp and mask it out, leaving only untraveled areas behind
    poly_oriented = orient(poly_stationary, 1)
    pts_sets = [np.array(poly_oriented.exterior.coords.xy).T[:-1]] + [np.array(interior.coords.xy).T[:-1] for interior in poly_oriented.interiors]

    for pts_stationary in pts_sets:
        # inside lacks a hueristic - wander until contained
        # first try and match up points
        for idx_stationary in range(pts_stationary.shape[0]):
            for idx_orbiting in range(pts_orbiting.shape[0]):
                startpoint = pts_stationary[idx_stationary] - pts_orbiting[idx_orbiting]
                poly_proposed = Polygon(pts_orbiting + startpoint)
                if poly_stationary.contains(poly_proposed) and \
                    poly_stationary_must_contain.contains(poly_proposed):
                    return startpoint

        # this may not succeed, so search by interpolation
        for idx_orbiting in range(pts_orbiting.shape[0]):
            offset_local = pts_orbiting[idx_orbiting]
            for position in np.linspace(0, 1, 24):
                startpoint = -offset_local + Polygon(pts_stationary).boundary.interpolate(position * Polygon(pts_stationary).boundary.length)
                poly_proposed = Polygon(pts_orbiting + startpoint)
                if poly_stationary.contains(poly_proposed) and \
                    poly_stationary_must_contain.contains(poly_proposed):
                    return startpoint

    # unable to find a start point
    return None


def compute_nfp(
        poly_stationary:Polygon,
        poly_orbiting:Polygon,
        inside:bool=True,
        plotting:str='prefix',
    ):
    polys_nfp = []
    pts_orbiting = poly_to_points(poly_orbiting, ccw=True)
    pts_stationary = poly_to_points(poly_stationary, ccw=True)
    if plotting:
        purge('.', f'step_\d+_\d+.*.png')

    for nfp_count in range(4):
        logging.info(f"Starting nfp {nfp_count}")

        # mark areas already covered by NFPs
        poly_nfp_cascaded = cascaded_union(polys_nfp)
        poly_stationary_must_contain = poly_stationary.difference(
            poly_nfp_cascaded.buffer(
                poly_nfp_cascaded.area ** 0.5 / 100,
                cap_style=2,
                join_style=2
            )
        )
        if not inside:
            # for outside, this is a good hueristic
            # also, here, holes do not matter
            idx_stationary_max = np.argmax(pts_stationary[:, 1])
            idx_orbiting_min = np.argmin(pts_orbiting[:, 1])
            startpoint = pts_stationary[idx_stationary_max] - pts_orbiting[idx_orbiting_min]
        else:
            # inside lacks a hueristic - wander until contained
            # here holes matter
            startpoint = inside_startpoint(poly_stationary, pts_orbiting, poly_stationary_must_contain)

        if startpoint is None:
            logging.info("Unable to find startpoint - stopping")
            break
        else:
            logging.info(f"Starting another loop {startpoint}")

        plotter(
            [poly_orbiting, poly_stationary, Point(startpoint).buffer(poly_stationary.area ** 0.5 / 10)],
            f"{plotting}_{nfp_count:0>4}_start.png"
        )

        offset_orbiting = np.copy(startpoint)
        reference = pts_orbiting[0] + offset_orbiting
        nfp = [
            np.copy(reference)
        ]

        start = np.copy(reference)
        for idx in range(2 * pts_orbiting.shape[0]):
            logging.info(f"NFP {nfp_count} Round {idx}")
            # find touching vertices/edges
            touching = generate_touching_pts(poly_stationary, pts_orbiting, offset_orbiting)
            assert not touching.empty

            # plotter(
            #     [poly_orbiting, poly_stationary] + ,
            #     f"n{nfp_count:0>4}_{idx:0>4}_touches.png"
            # )

            # generate translation vectors from the touching verts/edges
            vectors = vectors_from_touches(
                    touching,
                    poly_stationary,
                    pts_orbiting + offset_orbiting,
                    inside=inside
                )
            assert len(vectors) > 0


            # if plotting:
            #     plotter([
            #             poly_stationary,
            #             Polygon(pts_orbiting + offset_orbiting),
            #             LineString([pt_sliding, vector + pt_sliding]).buffer(0.05),
            #             Point(vector + pt_sliding).buffer(0.15),
            #         ], f'step_{idx:0>5}_{idx_vector:0>5}_proposed.png')

            # try vectors
            pt_new = None
            for idx_vector, vector_dct in enumerate(vectors):
                logging.info(f"Vector {idx} - {idx_vector + 1}/{len(vectors)}")
                vector = vector_dct['delta']
                pt_sliding = vector_dct['pt_sliding']
                # moves can collide by:
                #  * intersecting at final position
                #  * hitting on the way

                # if plotting:
                #     plotter([
                #             poly_stationary,
                #             Polygon(pts_orbiting + offset_orbiting),
                #             LineString([pt_sliding, vector + pt_sliding]).buffer(0.05),
                #             Point(vector + pt_sliding).buffer(0.15),
                #         ], f'step_{idx:0>5}_{idx_vector:0>5}_proposed.png')

                if not inside:
                    if poly_stationary.intersects(Polygon(pts_orbiting + offset_orbiting)):
                        intersection_result = poly_stationary.intersection(Polygon(pts_orbiting + offset_orbiting))
                        intersection_type = intersection_result.geometryType()
                        if intersection_type is 'GeometryCollection' or intersection_type is 'MultiPoint':
                            for item in intersection_result:
                                assert (item.geometryType() is 'Point' or item.geometryType() is 'LineString')
                        elif intersection_type is 'Polygon':
                            assert intersection_result.area < 1e-12, "Too much overlap"
                        else:
                            assert (intersection_type is 'Point' or intersection_type is 'LineString')
                else:
                    # ensure the new position does contain the polygon
                    # assert Polygon(pts_stationary).contains(Polygon(pts_orbiting + offset_orbiting))
                    # Polygon(pts_stationary).intersection(Polygon(pts_orbiting + offset_orbiting)).area
                    # Polygon(pts_orbiting + offset_orbiting).area
                    assert np.allclose(
                        poly_stationary.intersection(Polygon(pts_orbiting + offset_orbiting)).area,
                        Polygon(pts_orbiting + offset_orbiting).area
                    )
                    # plotter([Polygon(pts_stationary), Polygon(pts_orbiting + offset_orbiting)])

                if not no_collisions(poly_stationary, pts_orbiting, offset_orbiting, vector):
                    distance = slide_distance(
                        poly_stationary, \
                        pts_orbiting, offset_orbiting, \
                        vector,
                        inside=inside, startpoint=startpoint, idx=idx
                    )
                    if distance is None:
                        # No actual collisions found
                        logging.info("Found no real collisions - full move")
                        vector_adjusted = vector
                    elif np.allclose(distance, 0):
                        logging.info("Distance of 0. detected, skipping")
                        continue
                    else:
                        logging.info(f"Limited move to {distance:4.2f}")
                        vector_adjusted = vector * distance / np.linalg.norm(vector)
                else:
                    logging.info("Found no collisions - full move")
                    vector_adjusted = vector

                tested_result = poly_stationary.intersection(Polygon(pts_orbiting + offset_orbiting + vector_adjusted))
                # plotter([poly_stationary, Polygon(pts_orbiting + offset_orbiting + vector_adjusted)])
                if not inside:
                    if tested_result.area > 1e-16:
                        # plotter(
                        #     [poly_stationary, Polygon(pts_orbiting), tested_result],
                        #     f'step_{idx:0>5}_{idx_vector:0>5}_overlap.png'
                        # )
                        logging.info(f"Found a collision, overlap {tested_result.area} - aborting vector")
                        continue
                else:
                    if abs(tested_result.area - Polygon(pts_orbiting).area) > 1e-12:
                        # plotter(
                        #     [poly_stationary, Polygon(pts_orbiting), tested_result],
                        #     f'step_{idx:0>5}_{idx_vector:0>5}_escaping.png'
                        # )
                        logging.info(f"Found a collision, escaping - aborting vector")
                        continue


                # survived to here - the move is valid
                offset_orbiting_new = offset_orbiting + vector_adjusted
                pt_new = np.copy((pts_orbiting + offset_orbiting_new)[0])
                break


            if pt_new is None:
                raise Exception("No vectors found")

            if np.allclose(pt_new, nfp[0]):
                # closed the loop, all done
                logging.info("Found start point - Complete")
                break

            # mutation :(
            offset_orbiting = offset_orbiting_new
            nfp.append(pt_new)
            # All done with for loop

        poly_nfp = Polygon(nfp)
        polys_nfp.append(poly_nfp)

        if not inside:
            logging.info("Forcing stop at first run - outside can't have lobes")
            break

    # finally, cascaded_union
    # logging.info("REPEAT REPEAT")
    return cascaded_union(polys_nfp).boundary


# res = compute_nfp(poly_lobes, poly_diamond, inside=True)
2_606 - 1_778
2_606 - 1_778 - 250

def plot_areas(poly_stationary, poly_orbiting, boundary_linestring, name='orbit'):
    _ = plotter([
            poly_stationary,
            boundary_linestring,
        ] + [
            Polygon(poly_to_points(poly_orbiting) + np.array(
                boundary_linestring.interpolate(val).coords.xy
                ).T[0]
            ) for val in np.linspace(0,boundary_linestring.length,128)
        ], f"{name}_areas.png")
    pass

def cleanup():
    purge('.', 'step_\d+_\d+.*.png')
    purge('.', 'stationary_lines_\d+.*\.png')
    purge('.', 'orbit_lines_\d+.*\.png')

def purge(dir, pattern):
    for f in os.listdir(dir):
        if re.search(pattern, f):
            os.remove(os.path.join(dir, f))

def animate_possible(poly_stationary, poly_orbiting, boundary_linestring, name='orbit'):
    pts_orbiting = poly_to_points(poly_orbiting)

    purge('.', f'{name}_*.png')
    for idx, val in enumerate(np.linspace(0, boundary_linestring.length, 128)):
        plotter([
                poly_stationary,
                boundary_linestring,
            ] + \
            [ Polygon(pts_orbiting + np.array(
                    boundary_linestring.interpolate(val).coords.xy
                    ).T[0]
                )
            ], f'{name}_{idx:0>5}.png')

    normal = subprocess.run(f"convert -delay 3 -loop 0 {name}*.png {name}_cycle.gif".split(' '),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            check=True)
    # convert -delay 5 -loop 0 orbit*.png orbit_inside.gif
    purge('.', f'{name}_\d+.png')

def plot_all(poly_stationary, poly_orbiting, boundary_linestring, name='orbit'):
    poly_hash = md5(str(poly_stationary) + str(poly_orbiting))[:8]
    fullname = f"{name}_{poly_hash}"
    animate_possible(poly_stationary, poly_orbiting, boundary_linestring, name=fullname)
    plot_areas(poly_stationary, poly_orbiting, boundary_linestring, name=fullname)


# poly_square
# poly_strange
# poly_multi
# poly_lobes
# poly_diamond
# poly_tiny


diamond_inside_lobes = compute_nfp(poly_lobes, poly_diamond, inside=True)
cleanup()
plot_all(poly_lobes, poly_diamond, diamond_inside_lobes, name='diamond_inside_lobes')


tiny_outside_square = compute_nfp(poly_square, poly_tiny, inside=False)
cleanup()
plot_all(poly_square, poly_tiny, tiny_outside_square, name='tiny_outside_square')


# Not catching the stop point
tiny_inside_square = compute_nfp(poly_square, poly_tiny, inside=True)
cleanup()
plot_all(poly_square, poly_tiny, tiny_inside_square, name='tiny_inside_square')

strange_inside_lobes = compute_nfp(poly_lobes, poly_strange, inside=True)
cleanup()
plot_all(poly_lobes, poly_strange, strange_inside_lobes, name='strange_inside_lobes')

from shapely import affinity

junky = compute_nfp(poly_lobes, affinity.scale(affinity.rotate(poly_strange, 45), 1.2, 0.7), inside=True)
cleanup()
plot_all(poly_lobes, affinity.scale(affinity.rotate(poly_strange, 45), 1.2, 0.7), junky, name='junky')

affinity.scale(affinity.rotate(poly_strange, 45), 1.2, 0.7)
affinity.scale(affinity.rotate(poly_strange, 45), 1.2, 0.7).exterior.coords.xy
