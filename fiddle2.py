from shapely.geometry import Point, LineString, Polygon, MultiPolygon, MultiLineString, MultiPoint
from shapely.geometry.polygon import orient
from shapely.ops import cascaded_union
from shapely.affinity import translate, rotate
from shapely import ops
from shapely.wkt import dumps, loads

import numpy as np
import pandas as pd
from pandas.util.testing import assert_frame_equal
import matplotlib.pyplot as plt
from enum import Enum
import subprocess
import os, re
import logging
import hashlib
from typing import List, Set, Dict, Tuple, Optional
from functools import lru_cache
from itertools import combinations

logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)

def md5(contents):
    return hashlib.md5(contents.encode('utf-8')).hexdigest()

def hash_poly(poly):
    return int(md5(str(poly))[:8], 16)

class PolygonHashable(Polygon):
    def __hash__(self):
        if not hasattr(self, '_current_hash'):
            self._current_hash = hash_poly(self)
        return self._current_hash

class PointHashable(Point):
    def __hash__(self):
        if not hasattr(self, '_current_hash'):
            self._current_hash = hash_poly(self)
        return self._current_hash


def flatten(lst):
    return [item for sublist in lst for item in sublist]

def squish_linestring(item):
    if item.geometryType() is 'LineString':
        return [item]
    if item.geometryType() is 'MultiLineString':
        return list(item)
    raise Exception(f"Bad type {item.geometryType()}")

def zero_poly(poly):
    """
    Make the poly's first point zero
    """
    pts = np.array(poly.exterior.coords.xy).T
    pts_head = pts[0]
    poly_zeroed = translate(poly, xoff=-pts_head[0], yoff=-pts_head[1])
    return poly_zeroed, pts_head

def concave_drag(poly_start, poly_end):
    try:
        # this **HIDEOUS** hack has to do with geometry errors otherwise
        # TODO: fix this for real
        pts_orbit = poly_to_points(poly_start)
        pts_moved = poly_to_points(poly_end)
        assert pts_orbit.shape == pts_moved.shape
        hulls = []
        for idx in range(pts_orbit.shape[0]):
            ls = LineString([
                pts_orbit[idx],
                pts_moved[idx],
            ])
            if ls.length > 1e-9:
                hulls.append(ls.buffer(1e-9, cap_style=2,join_style=2))

        unioned = cascaded_union([poly_start.buffer(1e-9, cap_style=2,join_style=2), poly_end.buffer(1e-9, cap_style=2,join_style=2)] + hulls).buffer(-1e-9, cap_style=2,join_style=2)
        if unioned.geometryType() == 'MultiPolygon':
            # logging.warning("MULTI!")
            # logging.warning(f"{[un.area for un in unioned]}")
            # logging.warning(f"unioned = loads('{unioned.wkt}')")
            unioned = sorted(unioned, key=lambda i: i.area, reverse=False)[0]
        return Polygon(unioned.exterior)
    except Exception as e:
        hull_str = ",".join([f"loads('{hull.wkt}')" for hull in hulls])
        logging.info(f"hulls = [{hull_str}]")
        logging.warning(f"poly_start = loads('{poly_start.wkt}')")
        logging.warning(f"poly_end = loads('{poly_end.wkt}')")
        raise e


def plotter(items, filename=None, title=None):
    fig, axs = plt.subplots()
    fig.set_size_inches(5., 5.)
    axs.axis('equal')
    if title:
        plt.title(title)
    elif filename:
        plt.title(filename)
    for idx, item in enumerate(items):
        if item.geometryType() is 'LineString':
            axs.plot(*item.coords.xy, color='k')
            x, y = np.array(item.coords.xy).T[0]
            axs.scatter(x, y, s=60.05, marker='+', c='g', alpha=0.5)
            x, y = np.array(item.coords.xy).T[1]
            axs.scatter(x, y, s=60.05, marker='H', c='k', alpha=0.5)
        elif item.geometryType() is 'Point':
            axs.scatter(*item.coords.xy, s=24.05, marker='o', c='rgb'[idx % 3])
        elif item.geometryType() is 'MultiLineString':
            for linestring in item:
                axs.plot(*linestring.coords.xy, color='k')
        elif item.geometryType() is 'MultiPolygon':
            for poly in item:
                xs, ys = poly.exterior.xy
                axs.fill(xs, ys, alpha=0.5, fc='rgb'[idx % 3], ec='none')
        elif hasattr(item, 'interiors') and len(item.interiors) > 0:
            axs.plot(*item.exterior.xy, color='k')
            for interior in item.interiors:
                axs.plot(*interior.xy, color='r')
        else:
            xs, ys = item.exterior.xy
            axs.plot(*item.exterior.xy, color='k', linestyle='--', linewidth=0.5)
            axs.fill(xs, ys, alpha=0.5, fc='rgb'[idx % 3], ec='none')
    if filename is None:
        plt.show() #if interactive
    else:
        plt.savefig(filename)

poly_strange = PolygonHashable(np.array([[0,0],[1,0],[0.4,0.2],[1,1],[0,1],[0.2,0.4]]))
poly_strange

poly_square = PolygonHashable(np.array([[0,0],[1,0],[1,1],[0,1]]))
poly_square

poly_diamond = PolygonHashable(np.array([[0,0],[-1,1],[0,2],[1,1]]))
poly_diamond

poly_tiny = PolygonHashable(np.array([[0,0],[-1,1],[0,2],[1,1]]) * 0.2)
poly_tiny

poly_rectangle = PolygonHashable(np.array([[0,0],[4,0],[4,1],[0,1]]))
poly_rectangle

poly_inscribed = PolygonHashable(
        np.array([[0,0],[1,0],[1,1],[0,1]]),
        [
            np.array([[0.4,0.4],[0.6,0.4],[0.6,0.6],[0.4,0.6]]),
        ]
    )
poly_inscribed


poly_multi_shrunk_factor = 0.75
poly_multi = PolygonHashable(
        np.array([[0,0],[10,0],[10,10],[0,10]]) * poly_multi_shrunk_factor,
        [
            np.array([[2,2],[4,2],[4,4],[2,4]]) * poly_multi_shrunk_factor,
            np.array([[6,6],[8,6],[8,8],[6,8]]) * poly_multi_shrunk_factor,
        ]
    )
poly_multi

poly_lobes = PolygonHashable(
        np.array([[0,0],[10,0],[10,10],[0,10]]) * poly_multi_shrunk_factor,
        [
            np.array([[2,2],[4,2],[5,5],[2,4]]) * poly_multi_shrunk_factor,
            np.array([[5,5],[8,6],[8,8],[6,8]]) * poly_multi_shrunk_factor,
        ]
    )
poly_lobes


def idx_offset(idx, pts, delta=1):
    return (idx + delta) % pts.shape[0]

def pt_next_prev(pts, idx):
    """Return the point, the next, and the previous, handing wrap arounds"""
    idx_next = idx_offset(idx, pts, delta=1)
    idx_prev = idx_offset(idx, pts, delta=-1)

    pt = pts[idx]
    pt_next = pts[idx_next]
    pt_prev = pts[idx_prev]

    return pt, pt_next, pt_prev

class PointTriples(object):
    def __init__(self, points):
        self._points = points
    def __iter__(self):
        for idx in range(self._points.shape[0]):
            yield pt_next_prev(self._points, idx)

class PolyPairs(object):
    def __init__(self, polys):
        self._polys = polys
    def __iter__(self):
        for idx in range(len(self._polys)):
            poly_cur = self._polys[idx]
            poly_next = self._polys[(idx + 1) % len(self._polys)]
            yield cascaded_union([poly_cur, poly_next]).convex_hull

def orbiting_set(one_pass):
    return cascaded_union(list(PolyPairs(one_pass)))

def orbiting_sets(multiple_passes):
    """Using a list of lists of polys, draw the overwritten path"""
    return cascaded_union(list(map(orbiting_set, multiple_passes)))

def transform_poly(poly, point):
    return Polygon(
        poly_to_points(poly) + np.array(point.coords.xy).T[0]
    )

def poly_to_points(poly):
    return np.array(orient(poly).exterior.coords.xy).T[:-1]

def poly_interior_to_points(poly):
    return [
        np.array(orient(poly).exterior.coords.xy).T[:-1]
    ] + [np.array(interior.coords.xy).T[:-1] for interior in orient(poly).interiors]

def outside_startpoint(poly_stationary, poly_orbiting):
    pts_orbiting = poly_to_points(poly_orbiting)
    pts_stationary = poly_to_points(poly_stationary)
    idx_stationary_max = np.argmax(pts_stationary[:, 1])
    idx_orbiting_min = np.argmin(pts_orbiting[:, 1])
    return pts_stationary[idx_stationary_max] - pts_orbiting[idx_orbiting_min]

def inside_startpoint(poly_stationary, poly_orbiting, poly_stationary_must_contain, density=24):
    # NOTE: This needs to handle multi-polygons
    # It can do so by treating each linear ring as a potential placement
    # of course, it needs to elminate with multiple passes
    # Can take each nfp and mask it out, leaving only untraveled areas behind
    pts_orbiting = poly_to_points(poly_orbiting)
    pts_sets = poly_interior_to_points(poly_stationary)

    for pts_stationary in pts_sets:
        # inside lacks a hueristic - wander until contained
        # first try and match up points
        for idx_stationary in range(pts_stationary.shape[0]):
            for idx_orbiting in range(pts_orbiting.shape[0]):
                startpoint = pts_stationary[idx_stationary] - pts_orbiting[idx_orbiting]
                poly_proposed = Polygon(pts_orbiting + startpoint)
                if poly_stationary.contains(poly_proposed) and \
                    poly_stationary_must_contain.contains(poly_proposed):
                    return startpoint

        # this may not succeed, so search by interpolation
        for idx_orbiting in range(pts_orbiting.shape[0]):
            offset_local = pts_orbiting[idx_orbiting]
            for position in np.linspace(0, 1, density):
                startpoint = -offset_local + Polygon(pts_stationary).boundary.interpolate(position * Polygon(pts_stationary).boundary.length)
                poly_proposed = Polygon(pts_orbiting + startpoint)
                if poly_stationary.contains(poly_proposed) and \
                    poly_stationary_must_contain.contains(poly_proposed):
                    return startpoint

    # unable to find a start point
    return None

def d_abp(a, b, p):
    """
    Known in the literature as D-Function
    Given a vector a->b, determine side of point p
    +1 -> left, -1 -> right, 0=colinear
    """
    value = (a[0] - b[0]) * (a[1] - p[1]) - (a[1] - b[1]) * (a[0] - p[0])
    return value

def on_segment(head, toe, point, threshold=1e-6):
    distance = LineString([head, toe]).distance(Point(point))
    # logging.info(f'Distance {distance:4.3f}')
    if distance > threshold:
        return False
    # Must be on the line, but cannot touch the ends
    if Point(head).distance(Point(point)) < threshold:
        return False
    if Point(toe).distance(Point(point)) < threshold:
        return False
    return True


def nfp(
    poly_stationary:Polygon,
    poly_orbiting:Polygon,
    inside:bool,
    buffer:float=-1e-5,
    max_possible_interior_shapes:int=6,
    plot:bool=False
):
    poly_orbiting_zeroed, offset = zero_poly(poly_orbiting)
    result_raw = nfp_cached(
        poly_stationary=PolygonHashable(poly_stationary),
        poly_orbiting=PolygonHashable(poly_orbiting_zeroed),
        inside=inside,
        buffer=buffer,
        max_possible_interior_shapes=max_possible_interior_shapes,
        plot=plot,
    )
    return translate(result_raw, xoff=-offset[0], yoff=-offset[1])

@lru_cache(maxsize=2048,typed=True)
def nfp_cached(
    poly_stationary:PolygonHashable,
    poly_orbiting:PolygonHashable,
    inside:bool,
    buffer:float=-1e-5,
    max_possible_interior_shapes:int=6,
    plot:bool=False,
):
    assert buffer < 0
    polys_nfp = []
    orbiting_polys = []
    logging_info = logging.info if plot else (lambda s: None)

    for nfp_idx in range(max_possible_interior_shapes):

        if inside:
            poly_no_go = orbiting_sets(orbiting_polys)
            poly_contain = poly_stationary.difference(
                poly_no_go.buffer(
                    poly_stationary.area ** 0.5 / 100,
                    cap_style=2,
                    join_style=2
                )
            )
            startpoint = inside_startpoint(poly_stationary, poly_orbiting, poly_contain)
        else:
            startpoint = outside_startpoint(poly_stationary, poly_orbiting)

        if startpoint is None:
            logging_info(f"Unable to find start point at {nfp_idx} - stopping")
            break

        offset = startpoint
        startpoint_copy = np.copy(offset)
        nfp = []
        orbiting_polys.append([])
        # plotter([poly_stationary, poly_orbit], title='start')

        for point_count in range(2 * (poly_to_points(poly_orbiting).shape[0] + poly_to_points(poly_stationary).shape[0])):
            logging_info(f"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
            logging_info(f"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
            logging_info(f"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
            logging_info(f"%%%%%%   NFP {nfp_idx} - Pass {point_count:0>5}             %%%%%")
            logging_info(f"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

            pts_orbiting_offset = poly_to_points(poly_orbiting) + offset
            poly_orbit = Polygon(pts_orbiting_offset)
            poly_orbit_buffer = poly_orbit.buffer(buffer, cap_style=2, join_style=2)
            poly_orbit_buffer_out = poly_orbit.buffer(-buffer, cap_style=2, join_style=2)

            # poly_stationary_inlaid = poly_stationary.exterior.convex_hull.buffer(buffer, cap_style=2, join_style=2)
            # poly_stationary_exterior = poly_stationary.exterior.convex_hull.buffer(-buffer, cap_style=2, join_style=2)
            # poly_stationary_interiors = cascaded_union(
            #     [Polygon(inter).buffer(
            #         -buffer, cap_style=2, join_style=2
            #     ) for inter in poly_stationary.interiors]
            # )
            # poly_stationary_interior_test = poly_stationary_exterior.difference(poly_stationary_interiors)

            # tolerance more room, for checking if vectors really intersect
            poly_stationary_for_interior_intersections_with_vectors = Polygon(
                poly_stationary.exterior
            ).buffer(
                -buffer
            ).difference(
                cascaded_union([
                    Polygon(inner).buffer(-(buffer * 10)) for inner in poly_stationary.interiors
                ])
            )
            # tolerance with less room - making sure things fit
            poly_stationary_for_interior_containment = Polygon(
                poly_stationary.exterior
            ).buffer(
                buffer
            ).difference(
                cascaded_union([
                    Polygon(inner).buffer(buffer) for inner in poly_stationary.interiors
                ])
            )

            # plotter([poly_stationary, poly_orbit], title='Collision!')

            if inside:
                if not poly_stationary.contains(poly_orbit_buffer):
                    if plot:
                        plotter([poly_stationary, poly_orbit], title='Escaped!325')
                    raise Exception("Escaped TOP!")
            else:
                if poly_orbit.intersects(poly_stationary_for_interior_containment):
                    if plot:
                        plotter([poly_stationary, poly_orbit], title='Collision!347')
                    raise Exception("Collision TOP!")
            if len(nfp) > 0 and np.allclose(pts_orbiting_offset[0], nfp[0]):
                logging_info("FOund start loop - stopping")
                break
            # didn't fail smoke tests, count this as a position
            nfp.append(pts_orbiting_offset[0])
            orbiting_polys[-1].append(poly_orbit)

            logging_info(f"--------------------------------------")
            logging_info(f"--------------------------------------")
            logging_info(f"NFP {nfp_idx} of Point {point_count:0>3}")

            logging_info("Finding Contact Points")
            point_6s = []
            for idx_ring, pts_stationary in enumerate(poly_interior_to_points(poly_stationary)):
                for pt_s, pt_s_next, pt_s_prev in PointTriples(pts_stationary):
                    for pt_o, pt_o_next, pt_o_prev in PointTriples(poly_to_points(poly_orbit)):
                            # if plot:
                            #     plotter([
                            #         poly_stationary, poly_orbit,
                            #     ] + [
                            #         LineString([pt_s_prev, pt_s, pt_s_next]), LineString([pt_o_prev, pt_o, pt_o_next])
                            #     ], title=f"Testing Contact Point {pt_s}")

                        if np.allclose(pt_s, pt_o):
                            logging_info(f"Found point union {pt_s}")
                            pt_a, pt_b, pt_c = pt_s_prev, pt_s, pt_s_next
                            pt_d, pt_e, pt_f = pt_o_prev, pt_o, pt_o_next
                        elif on_segment(pt_s, pt_s_next, pt_o):
                            logging_info(f"Found orbit on {pt_s} {pt_o}")
                            pt_a, pt_b, pt_c = pt_s, pt_o, pt_s_next
                            pt_d, pt_e, pt_f = pt_o_prev, pt_o, pt_o_next
                        elif on_segment(pt_o, pt_o_next, pt_s):
                            logging_info(f"Found stationary on {pt_s} {pt_o}")
                            pt_a, pt_b, pt_c = pt_s_prev, pt_s, pt_s_next
                            pt_d, pt_e, pt_f = pt_o, pt_s, pt_o_next
                        else:
                            continue # ignore creation of vectors that don't match
                        point_6s.append([[pt_a, pt_b, pt_c], [pt_d, pt_e, pt_f], idx_ring > 0])
                        if plot:
                            plotter([
                                poly_stationary, poly_orbit,
                                Point(pt_b)
                            ] + [
                                LineString([pt_a, pt_b, pt_c]), LineString([pt_d, pt_e, pt_f])
                            ], title=f"Found Contact Point {pt_s}")

            assert len(point_6s) > 0, "Found no contact points"
            logging_info(f"Found {len(point_6s)} contact points")

            logging_info(f"--------------------------------------")
            logging_info(f"--------------------------------------")
            logging_info(f"NFP {nfp_idx} of Point {point_count:0>3} - Generating vectors")

            vectors = []
            for point_6_idx, point_6 in enumerate(point_6s):
                [pt_a, pt_b, pt_c], [pt_d, pt_e, pt_f], interior_ring = point_6
                d_result = d_abp(pt_b, pt_c, pt_f)

                if np.allclose(d_result, 0) or d_result > 0:
                    if np.allclose(d_result, 0):
                        logging_info(f'Contact point {point_6_idx} - Found Colinear Vector')
                    else:
                        logging_info(f'Contact point {point_6_idx} - Found Left Vector')
                    # left or colinear
                    if not inside:
                        vectors.append(pt_e - pt_f)
                    else:
                        vectors.append(pt_c - pt_b)
                        if interior_ring:
                            # Backwards check on interior
                            vectors.append(pt_e - pt_d)
                elif d_result < 0:
                    logging_info(f'Contact point {point_6_idx} - Found Right Vector')
                    # right
                    if not inside:
                        vectors.append(pt_c - pt_b)
                    else:
                        vectors.append(pt_e - pt_f)
                        if interior_ring:
                            vectors.append(pt_c - pt_a)

                logging_info(f"pt_a:{pt_a} pt_b:{pt_b} pt_c:{pt_c}")
                logging_info(f"pt_d:{pt_d} pt_e:{pt_e} pt_f:{pt_f} {interior_ring}")
                if plot:
                    plotter([
                        poly_stationary, poly_orbit,
                    ] + [
                        LineString([pt_a, pt_b, pt_c]), LineString([pt_d, pt_e, pt_f])
                    ], title=f"Contact point {point_6_idx} built")

            logging_info(f"Found {len(vectors)} vectors")

            logging_info(f"--------------------------------------")
            logging_info(f"--------------------------------------")
            logging_info(f"NFP {nfp_idx} of Point {point_count:0>3} - Checking vectors")

            moved = False
            for idx, vector in enumerate(vectors):
                logging_info(f"_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-")
                logging_info(f"Checking vector {idx + 1}/{len(vectors)} {vector}")
                logging_info(f"delta = np.array({list(vector)})")
                logging_info(f"poly_stationary = loads('{poly_stationary.wkt}')")
                logging_info(f"poly_orbit = loads('{poly_orbit.wkt}')")
                logging_info(f"ls = [LineString([pt, pt+delta]) for pt in poly_to_points(poly_orbit)]")
                logging_info(f"plotter([poly_stationary, poly_orbit] + ls)")


                # Generate line strings from poly points
                ls_vs_stationary = []
                ls_vs_orbit = []
                for pt_orbit in poly_to_points(poly_orbit):
                    ls_vs_stationary.append(LineString([pt_orbit, pt_orbit + vector]))
                if inside:
                    for pts_stationary in poly_interior_to_points(poly_stationary):
                        for pt_stationary in pts_stationary:
                            ls_vs_orbit.append(LineString([pt_stationary, pt_stationary - vector]))
                else:
                    for pt_stationary in poly_to_points(poly_stationary):
                        ls_vs_orbit.append(LineString([pt_stationary, pt_stationary - vector]))

                        if plot:
                            plotter(
                                [poly_stationary, poly_orbit] + ls_vs_stationary + ls_vs_orbit,
                                title=f"Vector Check {idx + 1}/{len(vectors)} {vector}"
                            )
                intersections_with_stationary, intersections_with_orbit = [], []
                for line_string in ls_vs_stationary: # from orbit, to collide with stationary
                    # NOTE: if inside and has interiors, the standard buffer doesn't work
                    # it pushes in the wrong direction, eliminating valid moves
                    # to fix this, the intersection must be made with the buffered exterior
                    # and then differenced with the negative buffered interiors

                    intersected = False
                    if inside:
                        line_with_interior = line_string.intersection(poly_stationary_for_interior_intersections_with_vectors)
                        # line_with_exterior = line_string.intersection(poly_stationary_exterior)
                        # line_with_interior = line_with_exterior.difference(poly_stationary_interiors)
                        logging_info(f"vs stationary {line_with_interior.geometryType()}")
                        if line_with_interior.geometryType() != 'GeometryCollection':
                            intersected = True
                    else:
                        if line_string.intersects(poly_stationary_for_interior_containment):
                            intersected = True

                    if intersected:
                        intersections_with_stationary.append(
                            (
                                line_string.intersection(poly_stationary),
                                np.array(line_string.coords.xy).T[0],
                                np.array(line_string.coords.xy).T[1],
                                False,
                            )
                        )
                for line_string in ls_vs_orbit:
                    if line_string.intersects(poly_orbit_buffer):
                        intersections_with_orbit.append(
                            (
                                # could make this orbit **slghtly** smaller, but... cause issues
                                line_string.intersection(poly_orbit), # What if the boundary contains the linestring? Could that be screened out?
                                # line_string.intersection(poly_orbit_buffer if inside else poly_orbit),
                                # line_string.intersection(poly_orbit_buffer if inside else poly_orbit_buffer_out),
                                np.array(line_string.coords.xy).T[0],
                                np.array(line_string.coords.xy).T[1],
                                True
                            )
                        )

                distance = np.linalg.norm(vector)
                intersections_all = intersections_with_stationary + intersections_with_orbit
                intersections_clean = [i for i in intersections_all if i[0].geometryType() is not 'GeometryCollection' or len(i[0]) > 0]
                count = len(intersections_clean)
                logging_info(f"_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-")
                logging_info(f'Starting at {distance:4.3f} on {count} real intersections (from {len(intersections_all)}) for {idx + 1}/{len(vectors)}')


                for local_idx, (intersection, pt_start, pt_end, on_orbit) in enumerate(intersections_clean):
                    if distance < 1e-4:
                        logging_info(f"Skipping intersection {local_idx + 1}/{count} - already 0 distance")
                        continue
                    logging_info(f"Checking intersection {local_idx + 1}/{count} | Orbit:{on_orbit}")
                    if intersection.geometryType() is 'GeometryCollection':
                        if len(intersection) < 1:
                            continue
                        intersection = [item for item in intersection if item.length > abs(buffer)][0]
                        # if a multi collection (ie line, pt or pt, line, pick the first)
                        # if plot:
                        #     plotter([
                        #         poly_stationary,
                        #         poly_orbit,
                        #         intersection[0],
                        #         intersection[1],
                        #     ])

                    if intersection.geometryType() is 'MultiPoint':
                        intersection = intersection[0]
                        # logging_info(f"{intersection.wkt}")
                        # raise Exception("Multipoint")

                    if intersection.geometryType() is 'Point':
                        if not inside:
                            if intersection.distance(Point(pt_start)) < 1e-6:
                                # start point is alright
                                logging_info('Intersection IS startpoint')
                                pass
                            else:
                                logging_info(f"Intersection point - distance from start {intersection.distance(Point(pt_start))}")
                        else: # inside
                            logging_info("Not handling points")
                            pass
                        continue
                    elif intersection.geometryType() is 'LineString':
                        # logging_info(" @@@@@@@ Intersection Line String")
                        # logging_info(f"{intersection.wkt}")
                        # logging_info(f"{LineString([pt_start, pt_end]).wkt}")
                        if not inside:
                            pt_picked = np.array(intersection.coords.xy).T[0]
                        else: # inside
                            # logging_info("Intersection Line String INSIDE")
                            if on_orbit:
                                # logging_info("Intersection Line String INSIDE ON_Orbit")
                                pt_picked = np.array(intersection.coords.xy).T[0]
                            else:
                                # logging_info("Intersection Line String INSIDE ON_stationary")
                                pt_picked = np.array(intersection.coords.xy).T[1]
                        # logging_info(f"{distance:4.3f}")
                        # logging_info(f"{distance_current:4.3f} vs {distance:4.3f} -> {min(distance_current, distance):4.3f}")
                        distance_current = np.linalg.norm(pt_start - pt_picked)
                        distance_old = distance
                        distance = min(distance_current, distance)
                        if plot:
                            plotter(
                                [
                                    poly_stationary,
                                    poly_orbit,
                                    intersection,
                                    Point(pt_picked),
                                    LineString([
                                        pt_start,
                                        pt_end
                                    ]),
                                ],
                                title=f"{local_idx+1}/{count} LStr vs{'Orb' if on_orbit else 'Sta'} {distance_old:4.3f}->{min(distance_current, distance):4.3f}"
                            )
                        continue
                    elif intersection.geometryType() is 'MultiLineString':
                        if len(intersection) < 1:
                            raise Exception('Insufficient MultiLine Strings')

                        # logging_info(f"{dumps(intersection)}")
                        # if the first bit is tiny AND the distance between the first tail and second head is ALSO tiny, just use the second
                        ls = [ls for ls in intersection if ls.length > abs(buffer)]
                        if len(ls) < 1:
                            logging_info(f"Unable to find any reasonable linestrings")
                            distance = 0.0
                            continue
                        target = ls[0]
                        # if len(intersection) > 1:
                        #     if intersection[0].length < abs(buffer * 1e-2) and intersection[0].distance(intersection[1]) < buffer * 1e-2:
                        #             logging_info("SKIPPING first tiny segment")
                        #             target = intersection[1]
                        #     else:
                        #         target = intersection[0]
                        # else:
                        #     target = intersection[0]


                        # logging_info(f"{dumps(intersection)}")
                        if on_orbit:
                            pt_picked = np.array(target.coords.xy).T[0]
                        else:
                            pt_picked = np.array(target.coords.xy).T[1]
                        # logging_info(f"{distance}->{min(distance_current, distance):4.3f}")
                        distance_current = np.linalg.norm(pt_start - pt_picked)
                        distance_old = distance
                        distance = min(distance_current, distance)
                        if plot:
                            plotter(
                                [
                                    poly_stationary,
                                    poly_orbit,
                                    intersection,
                                    LineString([
                                        pt_start,
                                        pt_end
                                    ]),
                                ],
                                title=f"{local_idx+1}/{count} MLStr vs{'Orb' if on_orbit else 'Sta'} {distance_old:4.3f}->{min(distance_current, distance):4.3f}"
                            )
                        continue
                    else:
                            # if plot:
                            #     plotter(
                            #         [
                            #         poly_stationary,
                            #         poly_orbit,
                            #         intersection,
                            #         LineString([
                            #             pt_start,
                            #             pt_end
                            #         ]),
                            #         ],
                            #         title=f"Point"
                            #     )
                        raise Exception(f"Unknown intersection - {intersection.geometryType()}")
                    raise Exception("Should never reach the base")

                logging_info(f"_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-")
                logging_info(f'Finished intersections with {distance:5.4f} for {idx + 1}/{len(vectors)}')

                if distance < 1e-4:
                    logging_info(f"Vector has no distance")
                    continue

                # # check if move intersects/touches start point
                ls_new = LineString([
                    pts_orbiting_offset[0],
                    pts_orbiting_offset[0] + vector * distance / np.linalg.norm(vector)
                ])
                if point_count > 0:
                    logging_info(f"Distance to point {Point(startpoint_copy).distance(ls_new)}")
                if point_count > 0 and Point(startpoint_copy).distance(ls_new) < abs(buffer):
                    distance = np.linalg.norm(pts_orbiting_offset[0] - startpoint_copy)
                    logging_info(f'Hit startpoint, set to {distance}')
                    # if plot:
                    #     plotter(
                    #         [
                    #             poly_stationary,
                    #             poly_orbit,
                    #             ls_new,
                    #             Point(startpoint_copy)
                    #         ],
                    #         title=f"startpoint"
                    #     )
                else:
                    logging_info("Did not hit startpoint")

                logging_info(f"_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-")
                logging_info(f'Valid move of length {distance:5.4f} for {idx + 1}/{len(vectors)}')

                scaled_vector = vector * distance / np.linalg.norm(vector)
                # try this move, final check
                if True:
                    logging_info(f"Final Check {idx + 1}/{len(vectors)}'")
                    offset_local = offset + scaled_vector
                    pts_orbiting_offset_local = poly_to_points(poly_orbiting) + offset_local
                    poly_orbit_local = Polygon(pts_orbiting_offset_local)
                    # poly_orbit_buffer_local = poly_orbit_local.buffer(buffer, cap_style=2, join_style=2)

                    # WARNING: This concave drag is **broken**
                    # # drag it out and check
                    # poly_dragged = concave_drag(
                    #     poly_orbit,
                    #     poly_orbit_local
                    # ).buffer(
                    #     buffer,
                    #     cap_style=2,
                    #     join_style=2
                    # )
                    this has issues when the poly isn't convex
                    poly_dragged = cascaded_union([
                        poly_orbit,
                        poly_orbit_local
                    ]).convex_hull.buffer(
                        buffer,
                        cap_style=2,
                        join_style=2
                    )
                    if inside:
                        # if not poly_stationary.contains(poly_dragged) and poly_dragged.difference(poly_stationary).area > abs(buffer):
                        if not poly_stationary.contains(poly_dragged):
                            if plot:
                                plotter([poly_stationary, poly_dragged], title='Escaped!650')
                            # logging_info(f"FOUND {point_count} {idx}")
                            # if point_count == 1 and idx == 2:
                            #     raise Exception("Escaped!")
                            logging_info(f"Aborting mistake! 774 {poly_dragged.difference(poly_stationary).area}")
                            continue
                    else:
                        # TODO: fix this to match the other intersection
                        # if poly_dragged.intersects(poly_stationary_exterior):
                        if poly_dragged.buffer(buffer).intersects(poly_stationary_for_interior_containment):
                            if plot:
                                logging.info(f"Overlap {poly_dragged.buffer(buffer).intersects(poly_stationary_for_interior_containment).area}")
                                logging.warning(f"poly_dragged = loads('{poly_dragged.wkt}')")
                                logging.warning(f"poly_stationary_for_interior_containment = loads('{poly_stationary_for_interior_containment.wkt}')")
                                # logging.warning(f"poly_stationary_interiors = loads('{poly_stationary_interiors.wkt}')")
                                # logging.warning(f"line_with_interior = loads('{line_with_interior.wkt}')")
                                logging.warning(f"poly_stationary = loads('{poly_stationary.wkt}')")
                                # plotter([poly_stationary_for_interior_containment, poly_orbit_buffer_local, poly_dragged], title='Collision!706')
                            logging_info("Aborting mistake! 788")
                            # raise Exception("Collision!")
                            continue

                moved = True
                break
            if not moved:
                logging_info(f"NFP {nfp_idx} of Point {point_count:0>3} - Failed")
                logging.warning(f"poly_stationary = loads('{poly_stationary}')")
                logging.warning(f"poly_orbiting = loads('{poly_orbiting}')")
                logging.warning(f"nfp(poly_stationary, poly_orbiting, {inside}, plot=True)")
                raise Exception("Unable to find valid move")
            ls_vs_stationary = []
            ls_vs_orbit = []
            for pt_orbit in poly_to_points(poly_orbit):
                ls_vs_stationary.append(LineString([pt_orbit, pt_orbit + scaled_vector]))
            for pt_stationary in poly_to_points(poly_stationary):
                ls_vs_orbit.append(LineString([pt_stationary, pt_stationary - scaled_vector]))

            if plot:
                plotter(
                    [
                        poly_stationary,
                        poly_orbit,
                        Polygon(poly_to_points(poly_orbiting) + offset + scaled_vector)
                    ] + ls_vs_stationary + ls_vs_orbit,
                    title=f"Final {nfp_idx}/{point_count:0>3} - {distance:4.3f} {scaled_vector}"
                )
            offset += scaled_vector
            logging_info("Next step!")

        poly_nfp = Polygon(nfp)
        if plot:
            plotter(
                [
                    poly_stationary,
                    poly_nfp.boundary
                ] + [
                    transform_poly(
                        poly_orbiting,
                        poly_nfp.boundary.interpolate(value * poly_nfp.boundary.length)
                    ) for value in np.linspace(0, 1, 64)
                ],
                title=f"Results"
            )
        polys_nfp.append(poly_nfp)

        # nfp polys loop
        if not inside:
            logging_info("Not inside, breaking instantly")
            break

    result = ops.linemerge([r.boundary for r in polys_nfp])
    if result.geometryType() is 'MultiLineString':
        return result
    if result.geometryType() is 'LineString':
        return MultiLineString([result])
    raise Exception(f"Invalid type: {result.geometryType()}")


def purge(dir, pattern):
    for f in os.listdir(dir):
        if re.search(pattern, f):
            os.remove(os.path.join(dir, f))

def plot_areas(poly_stationary, poly_orbiting, nfls, name='orbit', count=256):
    _ = plotter(
        [
            poly_stationary,
        ] + [
            transform_poly(
                poly_orbiting,
                nfls.interpolate(value * nfls.length)
            ) for value in np.linspace(0, 1, count)
        ],
        title=f"Swept Results {name}",
        filename=f"{name}_areas.png"
    )


def animate_possible(poly_stationary, poly_orbiting, nfls, name='orbit', count=256):
    pts_orbiting = poly_to_points(poly_orbiting)

    purge('.', f'temp_{name}_*.png')
    for idx, val in enumerate(np.linspace(0, nfls.length, count)):
        _ = plotter(
            [
                poly_stationary,
            ] + \
            [ Polygon(pts_orbiting + np.array(nfls.interpolate(val).coords.xy).T[0])],
            f'temp_{name}_{idx:0>5}.png'
        )

    normal = subprocess.run(f"convert -delay 3 -loop 0 temp_{name}*.png {name}_cycle.gif".split(' '),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            check=True)
    # convert -delay 5 -loop 0 orbit*.png orbit_inside.gif
    purge('.', f'temp_{name}_\d+.png')
    plt.close('all')

# logger.setLevel(logging.WARNING)

@lru_cache(maxsize=2048, typed=True)
def place_polys(
        poly_bin:PolygonHashable,
        polys_for_placement:Tuple[Tuple[PolygonHashable, PointHashable],...],
        center_of_gravity_pt:PointHashable=None,
        plot:bool=False,
    ):
    if len(polys_for_placement) < 1:
        return tuple()
    # TODO: better center of gravity
    center_of_gravity = Point(poly_bin.bounds[:2]) if center_of_gravity_pt is None else center_of_gravity_pt

    polys_for_placement_remaining, current_poly = polys_for_placement[:-1], polys_for_placement[-1]
    placed_polys_remaining = place_polys(poly_bin, polys_for_placement_remaining, center_of_gravity_pt, plot)

    # first, generate all the areas that can't be placed based on NFP of current
    # vs already placed
    mask_remove = cascaded_union([
            translate(
                nfp(poly, current_poly, False, plot=plot)[0],
                pt.coords.xy[0][0],
                pt.coords.xy[1][0],
            ).convex_hull for poly, pt in placed_polys_remaining
    ])
    # if mask_remove.geometryType() is not 'GeometryCollection':
    #     plotter([poly_bin, mask_remove], title=f"Mask Remove {idx:0>4}")

    # From each inside, remove if NFS of placements
    masked = [
        Polygon(nfls_item).difference(mask_remove) for nfls_item in \
        nfp(poly_bin, current_poly, True, plot=plot)
    ]
    masked_polys = [p for p in masked if p.geometryType() in ['Polygon', 'MultiPolygon']]
    # if plot:
    #     plotter([poly_bin] + masked_polys, title=f"Masked Polys {idx:0>4}")

    #
    nfls_available_0 = MultiLineString(
        flatten(
            [squish_linestring(nfls_item.boundary) for nfls_item in masked_polys]
        )
    )
    # cull anything outside the furthest option - prevent wandering outside
    # but what if there isn't an outer?

    # if the first contains the others, then yes, it's the outer container for intersection
    # otherwise, it's just part of the next fliter step
    # nfp(poly_bin, current_poly, True)[0].geometryType()
    # Polygon(nfp(poly_bin, current_poly, True)[0])
    # Polygon(nfp(poly_bin, current_poly, True)[0]).contains(nfp(poly_bin, current_poly, True)[1])

    first_contains_all = all(
        [
            Polygon(
                nfp(poly_bin, current_poly, True, plot=plot
            )[0]).contains(Polygon(item)) for item in nfp(
                poly_bin, current_poly, True
            )[1:]
        ]
    )

    if first_contains_all:
        nfls_available_1 = nfls_available_0.intersection(
            Polygon(
                nfp(poly_bin, current_poly, True, plot=plot)[0]
            ).buffer(1e-9, cap_style=2, join_style=2)
        )
        # eliminate anything that's already known as a no-go
        nfls_available_2 = nfls_available_1.difference(
            cascaded_union(
                [Polygon(poly) for poly in nfp(poly_bin, current_poly, True, plot=plot)[1:]]
            ).buffer(
                -1e-9, cap_style=2, join_style=2
            )
        )
    else:
        nfls_available_2 = nfls_available_0.difference(
            cascaded_union(
                [Polygon(poly) for poly in nfp(poly_bin, current_poly, True)]
            ).buffer(
                -1e-9, cap_style=2, join_style=2
            )
        )


    if nfls_available_2.geometryType() is not 'MultiLineString' and nfls_available_2.geometryType() is not 'LineString':
        # logging.info(f"Skipping polygon {len(polys_for_placement):0>4}, no placement options")
        return placed_polys_remaining

    if plot:
        plotter([poly_bin, nfls_available_2], title=f"Poly and Available locations {len(polys_for_placement):0>4}")

    distance_along = nfls_available_2.project(center_of_gravity)
    pt = nfls_available_2.interpolate(distance_along)
    if plot:
        plotter([poly_bin, nfls_available_2, Point(pt)], title=f"Poly and Picked location {len(polys_for_placement):0>4}")
    new_poly_tuple = (
            PolygonHashable(current_poly),
            PointHashable(np.array(pt.coords.xy).T[0])
        )
    placed_polys = placed_polys_remaining + (new_poly_tuple,)
    if plot:
        plotter(
            [poly_lobes, nfls_available_2] + \
            [
                translate(
                    poly,
                    pt.coords.xy[0][0],
                    pt.coords.xy[1][0],
                ) for poly, pt in placed_polys
            ],
            title=f'Placement {len(polys_for_placement):0>4}'
        )

    return placed_polys

# logger.setLevel(logging.INFO)
# logger.setLevel(logging.WARNING)
# nfp(member[0], member[0], False)
# nfp(poly_lobes, member[0], False)
# nfp(poly_lobes, member[0], True, plot=True)
# member[0]
# place_polys(poly_lobes, member, plot=True)
# logger.setLevel(logging.WARNING)

# polys_for_placement = ((poly_diamond,) * 5) + ((poly_square,) * 2) + ((poly_diamond,) * 5) + ((poly_square,) * 20)

def generate_population(poly_options, size, rs, count):
    return [tuple(rs.choice(poly_options, size)) for _ in range(count)]

def mutate(member, poly_options, rs, mutation_rate=0.05):
    flip = rs.random_sample(len(member)) > mutation_rate
    new_mutations = rs.choice(poly_options, len(member))
    return tuple(
        np.where(
            flip,
            member,
            new_mutations,
        )
    )
# mutate(polys_for_placement, poly_options, np.random.RandomState(451))

def crossover(member_a, member_b, rs):
    assert len(member_a) == len(member_b)
    cross_over_index = rs.randint(1, len(member_b)-1)
    new_a = tuple(member_a[:cross_over_index]) + tuple(member_b[cross_over_index:])
    new_b = tuple(member_b[:cross_over_index]) + tuple(member_a[cross_over_index:])
    return tuple(new_a), tuple(new_b)

# poly_options = [poly_diamond, poly_square]
poly_options = [rotate(poly_diamond, angle) for angle in np.arange(0, 180, 15)] + \
    [rotate(poly_square, angle) for angle in np.arange(0, 180, 15)] + \
    [rotate(poly_strange, angle) for angle in np.arange(0, 180, 15)]
poly_options = [rotate(poly_diamond, angle) for angle in np.arange(0, 180, 15)] + \
    [rotate(poly_square, angle) for angle in np.arange(0, 180, 15)]

# len(poly_options)
# HEREHERE

rs = np.random.RandomState(451)
population = generate_population(poly_options, 32, rs, 64)
max_generations = 3
for generation in range(max_generations):
    logging.info(f"Generation {generation} Started")
    solutions = [place_polys(poly_lobes, member, plot=False) for member in population]
    scores = [sum([item.area for item, _ in solution]) for solution in solutions]

    order_high_low = np.argsort(scores)[::-1]
    scores_top = np.array(scores)[order_high_low][:6]
    logging.info(f'Generation {generation} : {scores_top[:3]} : {np.max(scores):4.2f}/{np.mean(scores):4.2f}/{np.min(scores):4.2f}/{np.std(scores):4.2f}')
    if generation >= max_generations:
        logging.info("Complete")
        break
    survivors = np.array(solutions)[order_high_low][:6]
    initials = np.array(population)[order_high_low][:6]
    elites = tuple([tuple(i) for i in initials[:4]])
    crossovers = []
    comb = combinations(range(len(survivors)), 2)
    for member_a, member_b in comb:
        if member_a == member_b:
            continue
        a, b = crossover(initials[member_a], initials[member_b], rs)
        crossovers.append(a)
        crossovers.append(b)

    mutants = [mutate(member, poly_options, rs, 0.3) for member in list(initials) * 5]

    population = tuple(elites + tuple(crossovers) + tuple(mutants))

logging.info(f"Best score: {scores_top[0]:4.3f}")
scores_top

def plot_results(poly_bin, results, score):
    plotter(
        [poly_bin] + \
        [translate(poly, pt.coords.xy[0][0], pt.coords.xy[1][0]) for poly, pt in results],
        title=f'Placement with score {score:4.3f}'
    )

plot_results(poly_lobes, survivors[0], scores_top[0])


poly_stationary = loads('POLYGON ((0 0, 7.5 0, 7.5 7.5, 0 7.5, 0 0), (1.5 1.5, 3 1.5, 3.75 3.75, 1.5 3, 1.5 1.5), (3.75 3.75, 6 4.5, 6 6, 4.5 6, 3.75 3.75))')
poly_orbiting = loads('POLYGON ((0 0, -1.224744871391589 -0.7071067811865476, -1.931851652578137 0.5176380902050415, -0.7071067811865476 1.224744871391589, 0 0))')
nfp(poly_stationary, poly_orbiting, True, plot=True)

poly_stationary = loads('POLYGON ((1.112372435695794 0.1464466094067263, 0.8535533905932736 1.112372435695795, 0.8156596523969725 0.4810531309018494, -0.1123724356957947 0.8535533905932737, 0.1464466094067262 -0.1123724356957946, 0.6742382961596629 0.2361041566235316, 1.112372435695794 0.1464466094067263))')
poly_orbiting = loads('POLYGON ((0 0, -1.224744871391589 -0.7071067811865476, -1.931851652578137 0.5176380902050415, -0.7071067811865476 1.224744871391589, 0 0))')
nfp(poly_stationary, poly_orbiting, False, plot=True)

poly_stationary = loads('POLYGON ((1.112372435695794 0.1464466094067263, 0.8535533905932736 1.112372435695795, 0.8156596523969725 0.4810531309018494, -0.1123724356957947 0.8535533905932737, 0.1464466094067262 -0.1123724356957946, 0.6742382961596629 0.2361041566235316, 1.112372435695794 0.1464466094067263))')
poly_orbiting = loads('POLYGON ((0 0, -1.224744871391589 -0.7071067811865476, -1.931851652578137 0.5176380902050415, -0.7071067811865476 1.224744871391589, 0 0))')
nfp(poly_stationary, poly_orbiting, False, plot=True)


hulls = [loads('MULTIPOINT (1.819479216882342 -1.078298261984863, 1.112372435695794 0.1464466094067263, 3.044224088273931 -0.3711914807983152, 2.337117307087384 0.8535533905932737)'),loads('MULTIPOINT (1.112372435695794 0.1464466094067263, -0.1123724356957954 -0.5606601717798212, 2.337117307087384 0.8535533905932737, 1.112372435695794 0.1464466094067263)'),loads('MULTIPOINT (-0.1123724356957954 -0.5606601717798212, 0.5947343454907528 -1.78540504317141, 1.112372435695794 0.1464466094067263, 1.819479216882342 -1.078298261984863)'),loads('MULTIPOINT (0.5947343454907528 -1.78540504317141, 1.819479216882342 -1.078298261984863, 1.819479216882342 -1.078298261984863, 3.044224088273931 -0.3711914807983152)')]
poly_start = loads('POLYGON ((1.819479216882342 -1.078298261984863, 1.112372435695794 0.1464466094067263, -0.1123724356957954 -0.5606601717798212, 0.5947343454907528 -1.78540504317141, 1.819479216882342 -1.078298261984863))')
poly_end = loads('POLYGON ((3.044224088273931 -0.3711914807983152, 2.337117307087384 0.8535533905932737, 1.112372435695794 0.1464466094067263, 1.819479216882342 -1.078298261984863, 3.044224088273931 -0.3711914807983152))')
plotter([poly_start, poly_end])
cascaded_union([poly_start, poly_end] + hulls)
pts_orbit = poly_to_points(poly_start)
pts_moved = poly_to_points(poly_end)
assert pts_orbit.shape == pts_moved.shape
hulls = []
for idx in range(pts_orbit.shape[0]):
    ls = LineString([
        pts_orbit[idx],
        pts_moved[idx],
    ])
    if ls.length > 1e-9:
        hulls.append(ls.buffer(1e-9, cap_style=2,join_style=2))
hulls

# plotter([poly_start, poly_end] + hulls)
# plotter(hulls)
Polygon(cascaded_union([poly_start.buffer(1e-9, cap_style=2,join_style=2), poly_end.buffer(1e-9, cap_style=2,join_style=2)] + hulls).buffer(-1e-9, cap_style=2,join_style=2).exterior)
Polygon(cascaded_union([poly_start.buffer(1e-9, cap_style=2,join_style=2), poly_end.buffer(1e-9, cap_style=2,join_style=2)] + hulls).buffer(-1e-9, cap_style=2,join_style=2).exterior).geometryType()




MultiLineString([LineString([[0,0],[1,1]])]).exterior


hulls = [loads('MULTIPOINT (0.1464466094067262 -0.1123724356957948, -0.5606601717798214 1.112372435695794, 0.8535533905932738 -1.337117307087384, 0.1464466094067263 -0.1123724356957947)'),loads('MULTIPOINT (-0.5606601717798214 1.112372435695794, -1.78540504317141 0.4052656545092467, 0.1464466094067263 -0.1123724356957947, -1.078298261984863 -0.8194792168823422)'),loads('MULTIPOINT (-1.78540504317141 0.4052656545092467, -1.078298261984863 -0.8194792168823424, -1.078298261984863 -0.8194792168823422, -0.3711914807983151 -2.044224088273931)'),loads('MULTIPOINT (-1.078298261984863 -0.8194792168823424, 0.1464466094067262 -0.1123724356957948, -0.3711914807983151 -2.044224088273931, 0.8535533905932738 -1.337117307087384)')]
poly_start = loads('POLYGON ((0.1464466094067262 -0.1123724356957948, -0.5606601717798214 1.112372435695794, -1.78540504317141 0.4052656545092467, -1.078298261984863 -0.8194792168823424, 0.1464466094067262 -0.1123724356957948))')
poly_end = loads('POLYGON ((0.8535533905932738 -1.337117307087384, 0.1464466094067263 -0.1123724356957947, -1.078298261984863 -0.8194792168823422, -0.3711914807983151 -2.044224088273931, 0.8535533905932738 -1.337117307087384))')
junky = loads('MULTIPOLYGON (((-0.3711914807983151 -2.044224088273931, -1.078298261984863 -0.8194792168823424, -1.078298261984863 -0.8194792168823422, -1.078298261984863 -0.8194792168823423, -0.9252051530609141 -0.7310908692340239, 0.1464466094067263 -0.1123724356957947, 0.8535533905932738 -1.337117307087384, -0.3711914807983151 -2.044224088273931)), ((-1.078298261984863 -0.8194792168823422, -1.78540504317141 0.4052656545092467, -0.5606601717798214 1.112372435695794, 0.1464466094067263 -0.1123724356957947, 0.1464466094067263 -0.1123724356957947, -1.078298261984863 -0.8194792168823422)))')

plotter([poly_start, poly_end])
hc = [hull.convex_hull for hull in hulls]
[hull.convex_hull.area for hull in hulls]

plotter(hc[:2])
plotter(hc[:1])
plotter([hc[1]])
plotter([hc[2]])
plotter([hc[3]])
plotter([cascaded_union([poly_end, poly_start] + hc)])

concave_drag(poly_start, poly_end)
poly_start.is_simple
junky.is_valid


cascaded_union(list(junky))
cascaded_union(junky)


hulls = [loads('MULTIPOINT (0.1464466094067262 -0.1123724356957948, -0.5606601717798214 1.112372435695794, 0.8535533905932738 -1.337117307087384, 0.1464466094067263 -0.1123724356957947)'),loads('MULTIPOINT (-0.5606601717798214 1.112372435695794, -1.78540504317141 0.4052656545092467, 0.1464466094067263 -0.1123724356957947, -1.078298261984863 -0.8194792168823422)'),loads('MULTIPOINT (-1.78540504317141 0.4052656545092467, -1.078298261984863 -0.8194792168823424, -1.078298261984863 -0.8194792168823422, -0.3711914807983151 -2.044224088273931)'),loads('MULTIPOINT (-1.078298261984863 -0.8194792168823424, 0.1464466094067262 -0.1123724356957948, -0.3711914807983151 -2.044224088273931, 0.8535533905932738 -1.337117307087384)')]
poly_start = loads('POLYGON ((0.1464466094067262 -0.1123724356957948, -0.5606601717798214 1.112372435695794, -1.78540504317141 0.4052656545092467, -1.078298261984863 -0.8194792168823424, 0.1464466094067262 -0.1123724356957948))')
poly_end = loads('POLYGON ((0.8535533905932738 -1.337117307087384, 0.1464466094067263 -0.1123724356957947, -1.078298261984863 -0.8194792168823422, -0.3711914807983151 -2.044224088273931, 0.8535533905932738 -1.337117307087384))')

plotter([poly_start, poly_end])
concave_drag(poly_start, poly_end)

poly_stationary = loads('POLYGON ((0 0, 1 0, 0.4 0.2, 1 1, 0 1, 0.2 0.4, 0 0))')
poly_orbiting = loads('POLYGON ((0 0, -0.9659258262890682 0.2588190451025211, -0.4381341395361316 -0.08965754721680519, -1.224744871391589 -0.7071067811865472, -0.258819045102521 -0.9659258262890682, -0.2967127832988221 -0.3346065214951232, 0 0))')
nfp(poly_stationary, poly_orbiting, False, plot=True)

hulls = [loads('MULTIPOINT (1.965925826289068 -0.2588190451025211, 1 0, 1.464317512323252 0.07236986960972397, 0.4983916860341839 0.331188914712245)'),loads('MULTIPOINT (1 0, 1.527791686752937 -0.3484765923193263, 0.4983916860341839 0.331188914712245, 1.026183372787121 -0.01728767760708122)'),loads('MULTIPOINT (1.527791686752937 -0.3484765923193263, 0.7411809548974793 -0.9659258262890683, 1.026183372787121 -0.01728767760708122, 0.2395726409316632 -0.6347369115768233)'),loads('MULTIPOINT (0.7411809548974793 -0.9659258262890683, 1.707106781186547 -1.224744871391589, 0.2395726409316632 -0.6347369115768233, 1.205498467220731 -0.8935559566793443)'),loads('MULTIPOINT (1.707106781186547 -1.224744871391589, 1.669213042990246 -0.5934255665976442, 1.205498467220731 -0.8935559566793443, 1.16760472902443 -0.2622366518853992)'),loads('MULTIPOINT (1.669213042990246 -0.5934255665976442, 1.965925826289068 -0.2588190451025211, 1.16760472902443 -0.2622366518853992, 1.464317512323252 0.07236986960972397)')]
poly_start = loads('POLYGON ((1.965925826289068 -0.2588190451025211, 1 0, 1.527791686752937 -0.3484765923193263, 0.7411809548974793 -0.9659258262890683, 1.707106781186547 -1.224744871391589, 1.669213042990246 -0.5934255665976442, 1.965925826289068 -0.2588190451025211))')
poly_end = loads('POLYGON ((1.464317512323252 0.07236986960972397, 0.4983916860341839 0.331188914712245, 1.026183372787121 -0.01728767760708122, 0.2395726409316632 -0.6347369115768233, 1.205498467220731 -0.8935559566793443, 1.16760472902443 -0.2622366518853992, 1.464317512323252 0.07236986960972397))')

concave_drag(poly_end, poly_start)
hc = [hull.convex_hull for hull in hulls]
plotter(hc)
plotter([cascaded_union(hc + [poly_end, poly_start])])

item0 = loads('MULTIPOINT (1.965925826289068 -0.2588190451025211, 1 0, 1.464317512323252 0.07236986960972397, 0.4983916860341839 0.331188914712245)')
item1 = loads('MULTIPOINT (1 0, 1.527791686752937 -0.3484765923193263, 0.4983916860341839 0.331188914712245, 1.026183372787121 -0.01728767760708122)')
item2 = loads('MULTIPOINT (1.527791686752937 -0.3484765923193263, 0.7411809548974793 -0.9659258262890683, 1.026183372787121 -0.01728767760708122, 0.2395726409316632 -0.6347369115768233)')
item3 = loads('MULTIPOINT (0.7411809548974793 -0.9659258262890683, 1.707106781186547 -1.224744871391589, 0.2395726409316632 -0.6347369115768233, 1.205498467220731 -0.8935559566793443)')
item4 = loads('MULTIPOINT (1.707106781186547 -1.224744871391589, 1.669213042990246 -0.5934255665976442, 1.205498467220731 -0.8935559566793443, 1.16760472902443 -0.2622366518853992)')
item5 = loads('MULTIPOINT (1.669213042990246 -0.5934255665976442, 1.965925826289068 -0.2588190451025211, 1.16760472902443 -0.2622366518853992, 1.464317512323252 0.07236986960972397)')
poly_start = loads('POLYGON ((1.965925826289068 -0.2588190451025211, 1 0, 1.527791686752937 -0.3484765923193263, 0.7411809548974793 -0.9659258262890683, 1.707106781186547 -1.224744871391589, 1.669213042990246 -0.5934255665976442, 1.965925826289068 -0.2588190451025211))')
poly_end = loads('POLYGON ((1.464317512323252 0.07236986960972397, 0.4983916860341839 0.331188914712245, 1.026183372787121 -0.01728767760708122, 0.2395726409316632 -0.6347369115768233, 1.205498467220731 -0.8935559566793443, 1.16760472902443 -0.2622366518853992, 1.464317512323252 0.07236986960972397))')
items = [item0, item1, item2, item3, item4, item5]
hulls = [i.convex_hull for i in items]

plotter([poly_start, poly_end] + hulls)
cascaded_union(hulls + [poly_start, poly_end])
cascaded_union(hulls + [poly_start, poly_end]).geometryType()
plotter([poly_start, poly_end, cascaded_union(hulls + [poly_start, poly_end])])


poly_stationary = loads('POLYGON ((0 0, 1 0, 0.4 0.2, 1 1, 0 1, 0.2 0.4, 0 0))')
poly_orbiting = loads('POLYGON ((0 0, -0.9659258262890682 0.2588190451025211, -0.4381341395361316 -0.08965754721680519, -1.224744871391589 -0.7071067811865472, -0.258819045102521 -0.9659258262890682, -0.2967127832988221 -0.3346065214951232, 0 0))')
nfp(poly_stationary, poly_orbiting, False, plot=True)

poly_start = loads('POLYGON ((1.965925826289068 -0.2588190451025211, 1 0, 1.527791686752937 -0.3484765923193263, 0.7411809548974793 -0.9659258262890683, 1.707106781186547 -1.224744871391589, 1.669213042990246 -0.5934255665976442, 1.965925826289068 -0.2588190451025211))')
poly_end = loads('POLYGON ((1.464317512323252 0.07236986960972397, 0.4983916860341839 0.331188914712245, 1.026183372787121 -0.01728767760708122, 0.2395726409316632 -0.6347369115768233, 1.205498467220731 -0.8935559566793443, 1.16760472902443 -0.2622366518853992, 1.464317512323252 0.07236986960972397))')


poly_start = loads('POLYGON ((1.965925826289068 -0.2588190451025211, 1 0, 1.527791686752937 -0.3484765923193263, 0.7411809548974793 -0.9659258262890683, 1.707106781186547 -1.224744871391589, 1.669213042990246 -0.5934255665976442, 1.965925826289068 -0.2588190451025211))')
poly_end = loads('POLYGON ((1.464317512323252 0.07236986960972397, 0.4983916860341839 0.331188914712245, 1.026183372787121 -0.01728767760708122, 0.2395726409316632 -0.6347369115768233, 1.205498467220731 -0.8935559566793443, 1.16760472902443 -0.2622366518853992, 1.464317512323252 0.07236986960972397))')

poly_start = loads('POLYGON ((3.75 3.75, 3.491180954897479 4.715925826289069, 3.453287216701178 4.084606521495123, 2.525255128608411 4.457106781186548, 2.784074173710932 3.491180954897479, 3.311865860463868 3.839657547216806, 3.75 3.75))')
poly_end = loads('POLYGON ((3.765535119963634 3.692022142994043, 3.506716074861113 4.657947969283111, 3.468822336664811 4.026628664489166, 2.540790248572044 4.39912892418059, 2.799609293674565 3.433203097891522, 3.327400980427502 3.781679690210848, 3.765535119963634 3.692022142994043))')
poly_start = loads('POLYGON ((3.75 3.75, 3.491180954897479 4.715925826289069, 3.453287216701178 4.084606521495123, 2.525255128608411 4.457106781186548, 2.784074173710932 3.491180954897479, 3.311865860463868 3.839657547216806, 3.75 3.75))')
poly_end = loads('POLYGON ((3.765535119963634 3.692022142994043, 3.506716074861113 4.657947969283111, 3.468822336664811 4.026628664489166, 2.540790248572044 4.39912892418059, 2.799609293674565 3.433203097891522, 3.327400980427502 3.781679690210848, 3.765535119963634 3.692022142994043))')
hulls_ch = loads('MULTIPOLYGON (((2.799609293674565 3.433203097891522, 2.784074173710932 3.491180954897479, 3.311865860463868 3.839657547216806, 3.75 3.75, 3.506716074861113 4.657947969283111, 3.765535119963634 3.692022142994043, 3.327400980427502 3.781679690210848, 2.799609293674565 3.433203097891522)), ((2.540790248572044 4.39912892418059, 2.525255128608411 4.457106781186548, 3.453287216701178 4.084606521495123, 3.491180954897479 4.715925826289069, 3.506716074861113 4.657947969283111, 3.468822336664811 4.026628664489166, 2.540790248572044 4.39912892418059)))')
item0 = loads('MULTIPOINT (3.75 3.75, 3.491180954897479 4.715925826289069, 3.765535119963634 3.692022142994043, 3.506716074861113 4.657947969283111)')
item1 = loads('MULTIPOINT (3.491180954897479 4.715925826289069, 3.453287216701178 4.084606521495123, 3.506716074861113 4.657947969283111, 3.468822336664811 4.026628664489166)')
item2 = loads('MULTIPOINT (3.453287216701178 4.084606521495123, 2.525255128608411 4.457106781186548, 3.468822336664811 4.026628664489166, 2.540790248572044 4.39912892418059)')
item3 = loads('MULTIPOINT (2.525255128608411 4.457106781186548, 2.784074173710932 3.491180954897479, 2.540790248572044 4.39912892418059, 2.799609293674565 3.433203097891522)')
item4 = loads('MULTIPOINT (2.784074173710932 3.491180954897479, 3.311865860463868 3.839657547216806, 2.799609293674565 3.433203097891522, 3.327400980427502 3.781679690210848)')
item5 = loads('MULTIPOINT (3.311865860463868 3.839657547216806, 3.75 3.75, 3.327400980427502 3.781679690210848, 3.765535119963634 3.692022142994043)')
plotter([poly_start, poly_end] + list(item4))
plotter([poly_start, poly_end] + list(item0) + list(item1) + list(item4))


orient(poly_start)
orient(poly_end)
plotter([poly_start, poly_end])
concave_drag(poly_start, poly_end)
pts_orbit = poly_to_points(poly_start)
pts_moved = poly_to_points(poly_end)
assert pts_orbit.shape == pts_moved.shape
hulls = []
for idx in range(pts_orbit.shape[0]):
    idx_next = (idx + 1) % pts_orbit.shape[0]
    hull = MultiPoint([
        pts_orbit[idx],
        pts_orbit[idx_next],
        pts_moved[idx],
        pts_moved[idx_next],
    ]).convex_hull
    if hull.geometryType() == 'Polygon':
        hulls.append(hull)
plotter([poly_start, poly_end] + hulls)
Polygon(cascaded_union(hulls).exterior)
Polygon(cascaded_union(hulls).exterior).geometryType()

poly_start = loads('POLYGON ((0.258819045102521 1.965925826289068, -0.7071067811865472 2.224744871391589, -0.1793150944336107 1.876268279072263, -0.965925826289068 1.258819045102521, -5.551115123125783e-17 1, -0.03789373819630115 1.631319304793945, 0.258819045102521 1.965925826289068))')
poly_end = loads('POLYGON ((0.2967127832988221 1.334606521495123, -0.6692130429902461 1.593425566597644, -0.1414213562373096 1.244948974278318, -0.9280320880927668 0.627499740308576, 0.03789373819630104 0.3686806952060551, -5.551115123125783e-17 1, 0.2967127832988221 1.334606521495123))')
concave_drag(poly_start, poly_end)
pts_orbit = poly_to_points(poly_start)
pts_moved = poly_to_points(poly_end)
assert pts_orbit.shape == pts_moved.shape
hulls = []
for idx in range(pts_orbit.shape[0]):
    idx_next = (idx + 1) % pts_orbit.shape[0]
    hull = MultiPoint([
        pts_orbit[idx],
        pts_orbit[idx_next],
        pts_moved[idx],
        pts_moved[idx_next],
    ]).convex_hull
    if hull.geometryType() == 'Polygon':
        hulls.append(hull)
return Polygon(cascaded_union(hulls).exterior)
plotter([poly_start, poly_end])
hulls
plotter([poly_start, poly_end] + hulls)
plotter([poly_start, poly_end, Polygon(cascaded_union(hulls).exterior)])


cascaded_union(hulls).geometryType()
list(cascaded_union(hulls))
cascaded_union(hulls)[0]
Polygon(cascaded_union(hulls).exterior)



poly_start = loads('POLYGON ((1.965925826289068 -0.2588190451025211, 1 0, 1.527791686752937 -0.3484765923193263, 0.7411809548974793 -0.9659258262890683, 1.707106781186547 -1.224744871391589, 1.669213042990246 -0.5934255665976442, 1.965925826289068 -0.2588190451025211))')
poly_end = loads('POLYGON ((1.464317512323252 0.07236986960972397, 0.4983916860341839 0.331188914712245, 1.026183372787121 -0.01728767760708122, 0.2395726409316632 -0.6347369115768233, 1.205498467220731 -0.8935559566793443, 1.16760472902443 -0.2622366518853992, 1.464317512323252 0.07236986960972397))')
concave_drag(poly_start, poly_end)

pts_orbit = poly_to_points(poly_start)
pts_moved = poly_to_points(poly_end)
assert pts_orbit.shape == pts_moved.shape
hulls = []
for idx in range(pts_orbit.shape[0]):
    idx_next = (idx + 1) % pts_orbit.shape[0]
    hulls.append(MultiPoint([
        pts_orbit[idx],
        pts_orbit[idx_next],
        pts_moved[idx],
        pts_moved[idx_next],
    ]).convex_hull)

return Polygon(cascaded_union(hulls).exterior)


idx
cascaded_union(hulls)
plotter([poly_start, poly_end] + hulls)
plotter([poly_start, poly_end, concave_drag(poly_start, poly_end)])
plotter([poly_start, poly_end, Polygon(cascaded_union(hulls).exterior)])
idx_next

plotter([poly_start, poly_end, MultiPoint([
    pts_orbit[idx],
    pts_orbit[idx_next],
    pts_moved[idx],
    pts_moved[idx_next],
]).convex_hull])


0
    # hulls.append(MultiPoint(np.vstack([res[idx].reshape(2, -1), res[idx_next].reshape(2, -1)])).convex_hull)
hulls




[array([ 1.96592583, -0.25881905,  1.46431751,  0.07236987]), array([1.        , 0.        , 0.49839169, 0.33118891]), array([ 1.52779169, -0.34847659,  1.02618337, -0.01728768]), array([ 0.74118095, -0.96592583,  0.23957264, -0.63473691]), array([ 1.70710678, -1.22474487,  1.20549847, -0.89355596]), array([ 1.66921304, -0.59342557,  1.16760473, -0.26223665])]

poly_stationary = loads('POLYGON ((0 0, 7.5 0, 7.5 7.5, 0 7.5, 0 0), (1.5 1.5, 3 1.5, 3.75 3.75, 1.5 3, 1.5 1.5), (3.75 3.75, 6 4.5, 6 6, 4.5 6, 3.75 3.75))')
poly_orbiting = loads('POLYGON ((0 0, -0.2588190451025209 0.9659258262890684, -0.296712783298822 0.3346065214951232, -1.224744871391589 0.7071067811865475, -0.9659258262890683 -0.2588190451025209, -0.4381341395361316 0.08965754721680533, 0 0))')
nfp(poly_stationary, poly_orbiting, True, plot=True)

delta = np.array([1.5, 0.0])
poly_stationary = loads('POLYGON ((0 0, 7.5 0, 7.5 7.5, 0 7.5, 0 0), (1.5 1.5, 3 1.5, 3.75 3.75, 1.5 3, 1.5 1.5), (3.75 3.75, 6 4.5, 6 6, 4.5 6, 3.75 3.75))')
poly_orbit = loads('POLYGON ((4.5 6, 4.241180954897479 6.965925826289069, 4.203287216701178 6.334606521495123, 3.275255128608411 6.707106781186548, 3.534074173710932 5.741180954897479, 4.061865860463868 6.089657547216805, 4.5 6))')
ls = [LineString([pt, pt+delta]) for pt in poly_to_points(poly_orbit)]
lss = [LineString([pt, pt-delta]) for pt in poly_to_points(Polygon(poly_stationary.interiors[1]))]
plotter([poly_stationary, poly_orbit] + lss)
plotter([poly_stationary, poly_orbit] + [lss[3]])
lss[3].intersection(poly_orbit)
lss[3].intersection(poly_orbit).geometryType()
[item.length for item in lss[3].intersection(poly_orbit)]

poly_moved = translate(poly_orbit, xoff=delta[0], yoff=delta[1])
plotter([poly_orbit, poly_moved])

pts_orbit = poly_to_points(poly_orbit)
pts_moved = poly_to_points(poly_moved)
assert pts_orbit.shape == pts_moved.shape
res = np.hstack([pts_orbit, pts_moved]).reshape(-1, 2)
res = np.array([np.array([ 1.96592583, -0.25881905,  1.46431751,  0.07236987]), np.array([1.        , 0.        , 0.49839169, 0.33118891]), np.array([ 1.52779169, -0.34847659,  1.02618337, -0.01728768]), np.array([ 0.74118095, -0.96592583,  0.23957264, -0.63473691]), np.array([ 1.70710678, -1.22474487,  1.20549847, -0.89355596]), np.array([ 1.66921304, -0.59342557,  1.16760473, -0.26223665])])
res[0:4]
hulls = []
# for idx_start in range(res.shape[0]):
#     idx_end = (idx + 4) % res.shape[0]
#     hulls.append(MultiPoint(res[idx_start:idx_end]).convex_hull)


res[0]
hulls = []
for idx in range(res.shape[0]):
    idx_next = (idx + 1) % res.shape[0]
    hulls.append(MultiPoint(np.vstack([res[idx].reshape(2, -1), res[idx_next].reshape(2, -1)])).convex_hull)
hulls
[p.area for p in hulls]
plotter([] + hulls)
#
# def concave_drag(poly_start, poly_end):
#     pts_orbit = poly_to_points(poly_orbit)
#     pts_moved = poly_to_points(poly_moved)
#     assert pts_orbit.shape == pts_moved.shape
#     res = np.hstack([pts_orbit, pts_moved])
#     hulls = []
#     for idx in range(res.shape[0]):
#         idx_next = (idx + 1) % res.shape[0]
#         hulls.append(MultiPoint(np.vstack([res[idx].reshape(2, -1), res[idx_next].reshape(2, -1)])).convex_hull)
#     return cascaded_union(hulls)

plotter([poly_orbit, poly_moved, concave_drag(poly_orbit, poly_moved)])
cascaded_union(hulls)

ls = [ls for ls in ls[3].intersection(poly_orbit) if ls.length > abs(1e-6)]
ls[0]
ls[3].intersection(poly_orbit.buffer(-1e-8))
0
