import shapely
from shapely.geometry import Point, Polygon, LineString
import shapely.affinity
import matplotlib.pyplot as plt
import numpy as np
import math

def plotter(items):
    fig, axs = plt.subplots()
    fig.set_size_inches(5., 5.)
    for idx, item in enumerate(items):
        xs, ys = item.exterior.xy
        axs.fill(xs, ys, alpha=0.5, fc='rgb'[idx % 3], ec='none')
    plt.show() #if not interactive


a = Point(2, 2)
b = Polygon([[1, 1], [1, 3], [3, 3], [3, 1]])
c = Polygon([[0, 0], [0, 4], [4, 4], [4, 0]])
d = Point(-1, -1)


b
c

b.intersects(c)
b.contains(c)
c.contains(b)

plotter([b, c, shapely.affinity.translate(b, xoff=1.0, yoff=0.0)])


ba = Polygon([[-1, 1], [-1, 3], [5, 3], [5, 1]])
ca = Polygon([[0, 0], [0, 4], [4, 4], [4, 0]])


plotter([ba, ca])
res = ba.difference(ca)
res[0]
res[1]
len(res)
type(res)

plotter([res[0], res[1]])


package = Polygon([[0, 0], [0, 1], [1, 1], [1, 0]])
bin = Polygon([[0, 0], [0, 4], [4, 4], [4, 0]])
plotter([bin, package])
package = Polygon([[0, 0], [0, 1], [1, 1], [1, 0]])
bin = Polygon([[0, 0], [1, 4], [4, 4], [4, 0]])
plotter([bin, package])

for a, b in zip(bin.exterior.coords, bin.exterior.coords[1:]):
    break
np.array(b) - np.array(a)



q = Polygon(np.array([[0, 0], [0.3, 2-0.3], [2, 2], [0.3, 2.3], [0, 4], [-2, 2]]))
q = Polygon(np.array([
    [0, 0], [0.3, 2-0.3],
    [2, 2], [0.3, 2.3],
    [0, 4], [-0.3, 2.3],
    [-2, 2], [-0.3, 2-0.3]]))
q


def almostEqual(a, b, tolerance=1e-9):
    return abs(a - b) < tolerance

def almostEqualPts(a, b, tolerance=1e-9):
    return almostEqual(a['x'], b['x'], tolerance) and almostEqual(a['y'], b['y'], tolerance)

def normalizeVector(v):
    mag = math.sqrt(v['x']*v['x'] + v['y']*v['y'])
    inverse = 1/mag

    return {
        'x': v['x']*inverse,
        'y': v['y']*inverse
    }


def pointDistance(p, s1, s2, normal, infinite=False):
    normal = normalizeVector(normal)

    dir = {
        'x': normal['y'],
        'y': -normal['x'],
    }

    pdot = p['x']*dir['x'] + p['y']*dir['y']
    s1dot = s1['x']*dir['x'] + s1['y']*dir['y']
    s2dot = s2['x']*dir['x'] + s2['y']*dir['y']

    pdotnorm = p['x']*normal['x'] + p['y']*normal['y']
    s1dotnorm = s1['x']*normal['x'] + s1['y']*normal['y']
    s2dotnorm = s2['x']*normal['x'] + s2['y']*normal['y']

    if not infinite:
        if (((pdot<s1dot or almostEqual(pdot, s1dot)) and (pdot<s2dot or almostEqual(pdot, s2dot))) or ((pdot>s1dot or almostEqual(pdot, s1dot)) and (pdot>s2dot or almostEqual(pdot, s2dot)))):
            return None # dot doesn't collide with segment, or lies directly on the vertex

        if ((almostEqual(pdot, s1dot) and almostEqual(pdot, s2dot)) and (pdotnorm>s1dotnorm and pdotnorm>s2dotnorm)):
            return min(pdotnorm - s1dotnorm, pdotnorm - s2dotnorm)

        if ((almostEqual(pdot, s1dot) and almostEqual(pdot, s2dot)) and (pdotnorm<s1dotnorm and pdotnorm<s2dotnorm)):
            return -min(s1dotnorm-pdotnorm, s2dotnorm-pdotnorm)

    return -(pdotnorm - s1dotnorm + (s1dotnorm - s2dotnorm)*(s1dot - pdot)/(s1dot - s2dot))



def segmentDistance(A, B, E, F, direction):
    normal = {
        'x': direction['y'],
        'y': -direction['x'],
    }

    reverse = {
        'x': -direction['x'],
        'y': -direction['y'],
    }

    dotA = A['x']*normal['x'] + A['y']*normal['y']
    dotB = B['x']*normal['x'] + B['y']*normal['y']
    dotE = E['x']*normal['x'] + E['y']*normal['y']
    dotF = F['x']*normal['x'] + F['y']*normal['y']

    crossA = A['x']*direction['x'] + A['y']*direction['y']
    crossB = B['x']*direction['x'] + B['y']*direction['y']
    crossE = E['x']*direction['x'] + E['y']*direction['y']
    crossF = F['x']*direction['x'] + F['y']*direction['y']

    crossABmin = min(crossA,crossB)
    crossABmax = max(crossA,crossB)

    crossEFmax = max(crossE,crossF)
    crossEFmin = min(crossE,crossF)

    ABmin = min(dotA,dotB)
    ABmax = max(dotA,dotB)

    EFmax = max(dotE,dotF)
    EFmin = min(dotE,dotF)

    # // segments that will merely touch at one point
    if almostEqual(ABmax, EFmin) or almostEqual(ABmin, EFmax):
        return None
    # // segments miss eachother completely
    if (ABmax < EFmin) or (ABmin > EFmax):
        return None


    if (ABmax > EFmax and ABmin < EFmin) or (EFmax > ABmax and EFmin < ABmin):
        overlap = 1
    else:
        minMax = min(ABmax, EFmax)
        maxMin = max(ABmin, EFmin)

        maxMax = max(ABmax, EFmax)
        minMin = min(ABmin, EFmin)

        overlap = (minMax-maxMin)/(maxMax-minMin)


    crossABE = (E['y'] - A['y']) * (B['x'] - A['x']) - (E['x'] - A['x']) * (B['y'] - A['y'])
    crossABF = (F['y'] - A['y']) * (B['x'] - A['x']) - (F['x'] - A['x']) * (B['y'] - A['y'])

    # // lines are colinear
    if almostEqual(crossABE,0) and almostEqual(crossABF,0):

        ABnorm = {'x': B['y']-A['y'], 'y': A['x']-B['x']}
        EFnorm = {'x': F['y']-E['y'], 'y': E['x']-F['x']}

        ABnormlength = math.sqrt(ABnorm['x']*ABnorm['x'] + ABnorm['y']*ABnorm['y'])
        ABnorm['x'] /= ABnormlength
        ABnorm['y'] /= ABnormlength

        EFnormlength = math.sqrt(EFnorm['x']*EFnorm['x'] + EFnorm['y']*EFnorm['y'])
        EFnorm['x'] /= EFnormlength
        EFnorm['y'] /= EFnormlength

        # // segment normals must point in opposite directions
        if (abs(ABnorm['y'] * EFnorm['x'] - ABnorm['x'] * EFnorm['y']) < 1e-6) and \
            (ABnorm['y'] * EFnorm['y'] + ABnorm['x'] * EFnorm['x'] < 0):
            # // normal of AB segment must point in same direction as given direction vector
            normdot = ABnorm['y'] * direction['y'] + ABnorm['x'] * direction['x']
            # // the segments merely slide along eachother
            if almostEqual(normdot, 0):
                return None
            if normdot < 0:
                return 0
        return None


    distances = []

    # // coincident points
    if almostEqual(dotA, dotE):
        distances.append(crossA-crossE)
    elif almostEqual(dotA, dotF):
        distances.append(crossA-crossF)
    elif (dotA > EFmin) and (dotA < EFmax):
        d = pointDistance(A,E,F,reverse)

        if d is not None and almostEqual(d, 0):
            # //  A currently touches EF, but AB is moving away from EF
            dB = pointDistance(B,E,F,reverse,True)
            if dB < 0 or almostEqual(dB*overlap,0):
                d = None

        if d is not None:
            distances.append(d)

    if almostEqual(dotB, dotE):
        distances.append(crossB-crossE)
    elif almostEqual(dotB, dotF):
        distances.append(crossB-crossF)
    elif dotB > EFmin and dotB < EFmax:
        d = pointDistance(B,E,F,reverse)

        if d is not None and almostEqual(d, 0):
            # // crossA>crossB A currently touches EF, but AB is moving away from EF
            dA = pointDistance(A,E,F,reverse,True)
            if dA < 0 or almostEqual(dA*overlap,0):
                d = None
        if d is not None:
            distances.append(d)

    if dotE > ABmin and dotE < ABmax:
        d = pointDistance(E,A,B,direction)
        if d is not None and almostEqual(d, 0):
             # // crossF<crossE A currently touches EF, but AB is moving away from EF
            dF = pointDistance(F,A,B,direction, True)
            if dF < 0 or almostEqual(dF*overlap,0):
                d = None
        if d is not None:
            distances.append(d)

    if dotF > ABmin and dotF < ABmax:
        d = pointDistance(F,A,B,direction)
        if d is not None and almostEqual(d, 0):
            # // and crossE<crossF A currently touches EF, but AB is moving away from EF
            dE = pointDistance(E,A,B,direction, True)
            if dE < 0 or almostEqual(dE*overlap,0):
                d = None
        if d is not None:
            distances.append(d)

    if len(distances) == 0:
        return None

    return min(distances)



# returns True if p lies on the line segment defined by AB, but not at any endpoints
# **certainly** need work!
def onSegment(A,B,p,decimal=6):
    start = Point(A['x'], A['y'])
    end = Point(B['x'], B['y'])
    pt = Point(p['x'], p['y'])
    if start.almost_equals(pt, decimal=decimal):
        return False
    if end.almost_equals(pt, decimal=decimal):
        return False
    segment = LineString([start, end])
    return segment.distance(pt) < 10 ** -decimal

def pairify(lst):
    return zip(lst[:-1], lst[1:])

def rotate(l, n=1):
    """Rotate a list by a value"""
    lst = list(l)
    return lst[n:] + lst[:n]

# // return true if point is in the polygon, false if outside, and null if exactly on a point or edge
# redo with numpy or shapely?
def pointInPolygon(point, polygon, polygon_dct):
    if len(polygon) < 3:
        raise Exception("Not a polygon")

    inside = False
    offsetx = polygon_dct['offsetx'] if 'offsetx' in polygon_dct else 0
    offsety = polygon_dct['offsety'] if 'offsety' in polygon_dct else 0

    # this provides tuples of current (i) and previous (j)
    idxs = range(len(polygon))
    for i, j in list(zip(idxs, rotate(idxs,-1))):
        xi = polygon[i]['x'] + offsetx
        yi = polygon[i]['y'] + offsety
        xj = polygon[j]['x'] + offsetx
        yj = polygon[j]['y'] + offsety

        if almostEqual(xi, point['x']) and almostEqual(yi, point['y']):
            return None # no result

        if onSegment({'x': xi, 'y': yi}, {'x': xj, 'y': yj}, point):
            return None # exactly on the segment

        if almostEqual(xi, xj) and almostEqual(yi, yj):
        # // ignore very small lines
            continue

        intersect = ((yi > point['y']) != (yj > point['y'])) and \
                    (point['x'] < (xj - xi) * (point['y'] - yi) / (yj - yi) + xi)
        if intersect:
            inside = not inside

    return inside

def intersect(A,B,A_dct,B_dct):
    # guessed implementation
    A_poly = Polygon(np.stack([
        np.array([dct['x'] for dct in A]) + A_dct['offsetx'],
        np.array([dct['y'] for dct in A]) + A_dct['offsety'],
    ]).T)
    B_poly = Polygon(np.stack([
        np.array([dct['x'] for dct in B]) + B_dct['offsetx'],
        np.array([dct['y'] for dct in B]) + B_dct['offsety'],
    ]).T)
    return A_poly.intersects(B_poly)

# // returns true if point already exists in the given nfp
def inNfp(p, nfp):
    if nfp is None or len(nfp) == 0:
        return False

    for nfp_0 in nfp:
        for nfp1 in nfp0:
            if almostEqual(p['x'], nfp1['x']) and almostEqual(p['y'], nfp1['y']):
                return True
    return False

# // project each point of B onto A in the given direction, and return the
def polygonProjectionDistance(A_lst, B_lst, A_dct, B_dct, direction):

    Aoffsetx = A_dct['offsetx'] if 'offsetx' in A_dct else 0.
    Aoffsety = A_dct['offsety'] if 'offsety' in A_dct else 0.

    Boffsetx = B_dct['offsetx'] if 'offsetx' in B_dct else 0.
    Boffsety = B_dct['offsety'] if 'offsety' in B_dct else 0.

    A = list(A_lst)
    B = list(B_lst)

    # close the loop for polygons
    if not almostEqualPts(A[0], A[-1]):
        A.append(dict(A[0]))
    if not almostEqualPts(B[0], B[-1]):
        B.append(dict(A[0]))


    edgeA = A
    edgeB = B

    distance = None
    for i in range(len(edgeB)):
        # // the shortest/most negative projection of B onto A
        minprojection = None
        minp = None
        for j in range(len(edgeA) - 1):
            p = {'x': edgeB[i]['x'] + Boffsetx, 'y': edgeB[i]['y'] + Boffsety}
            s1 = {'x': edgeA[j]['x'] + Aoffsetx, 'y': edgeA[j]['y'] + Aoffsety}
            s2 = {'x': edgeA[j+1]['x'] + Aoffsetx, 'y': edgeA[j+1]['y'] + Aoffsety}

            if abs((s2['y']-s1['y']) * direction['x'] - (s2['x']-s1['x']) * direction['y']) < 1e-6:
                continue


            # // project point, ignore edge boundaries
            d = pointDistance(p, s1, s2, direction)

            if (d is not None and (minprojection is None or d < minprojection)):
                minprojection = d
                minp = p

        if (minprojection is not None and (distance is None or minprojection > distance)):
            distance = minprojection

    return distance



# // searches for an arrangement of A and B such that they do not overlap
# // if an NFP is given, only search for startpoints that have not already been
# traversed in the given NFP
def searchStartPoint(A,B,B_dct,inside,NFP=None):
    A = list(A)
    B = list(B)

    Boffsetx = B_dct['offsetx']
    Boffsety = B_dct['offsety']

    # // close the loop for polygons
    if not almostEqualPts(A[0], A[-1]):
        A.append(dict(A[0]))
    if not almostEqualPts(B[0], B[-1]):
        B.append(dict(A[0]))

    for i in range(len(A)):
        if not A[i]['marked']:
            A[i]['marked'] = True
            for j in range(len(B)):
                Boffsetx = A[i]['x'] - B[j]['x']
                Boffsety = A[i]['y'] - B[j]['y']

                Binside = None
                for k in range(len(B)):
                    inpoly = pointInPolygon(
                        {'x': B[k]['x'] + Boffsetx, 'y': B[k]['y'] + Boffsety},
                        A,
                        A_dct
                    )
                    if inpoly is not None:
                        Binside = inpoly
                        break

                if Binside is None:
                    # A and B are the same
                    return None

                startPoint = {'x': Boffsetx, 'y': Boffsety}
                if (
                    ((Binside and inside) or (not Binside and not inside)) and \
                     not intersect(A,B,A_dct,B_dct) and \
                     not inNfp(startPoint, NFP)
                     ):
                    return startPoint

                # // slide B along vector
                vx = A[i+1]['x'] - A[i]['x']
                vy = A[i+1]['y'] - A[i]['y']

                d1 = polygonProjectionDistance(A,B,A_dct,B_dct,{'x': vx, 'y': vy})
                d2 = polygonProjectionDistance(B,A,B_dct,A_dct,{'x': -vx, 'y': -vy})

                d = None

                # // todo: clean this up
                if (d1 is None and d2 is None):
                    pass
                elif (d1 is None):
                    d = d2
                elif (d2 is None):
                    d = d1
                else:
                    d = min(d1,d2)

                # // only slide until no longer negative
                # // todo: clean this up
                if(d is not None and not almostEqual(d,0) and d > 0):
                    pass
                else:
                    continue

                vd2 = vx*vx + vy*vy

                if (d*d < vd2 and not almostEqual(d*d, vd2)):
                    vd = math.sqrt(vx*vx + vy*vy)
                    vx *= d/vd
                    vy *= d/vd

                Boffsetx += vx
                Boffsety += vy

                for k in range(len(B)):
                    inpoly = pointInPolygon({'x': B[k]['x'] + Boffsetx, 'y': B[k]['y'] + Boffsety}, A, A_dct)
                    if(inpoly is not None):
                        Binside = inpoly
                        break

                startPoint = {'x': Boffsetx, 'y': Boffsety}
                if (
                    ((Binside and inside) or (not Binside and not inside)) and \
                    not intersect(A,B) and \
                    not inNfp(startPoint, NFP)
                    ):
                    return startPoint

    return None


def polygonSlideDistance(A_lst, B_lst, A_dct, B_dct, direction, ignoreNegative):

    Aoffsetx = A_dct['offsetx'] if 'offsetx' in A_dct else 0
    Aoffsety = A_dct['offsety'] if 'offsety' in A_dct else 0

    Boffsetx = B_dct['offsetx'] if 'offsetx' in B_dct else 0
    Boffsety = B_dct['offsety'] if 'offsety' in B_dct else 0

    A = list(A_lst)
    B = list(B_lst)

    # close the loop for polygons
    if not almostEqualPts(A[0], A[-1]):
        A.append(dict(A[0]))
    if not almostEqualPts(B[0], B[-1]):
        B.append(dict(A[0]))

    edgeA = A
    edgeB = B
    distance = None
    dir = normalizeVector(direction)

    normal = {
        'x': dir['y'],
        'y': -dir['x']
    }

    reverse = {
        'x': -dir['x'],
        'y': -dir['y'],
    }

    for edgerB, edgerBNext in pairify(edgeB):
        mind = None
        for edgerA, edgerANext in pairify(edgeA):
            A1 = {'x': edgerA['x'] + Aoffsetx, 'y': edgerA['y'] + Aoffsety}
            A2 = {'x': edgerANext['x'] + Aoffsetx, 'y': edgerANext['y'] + Aoffsety}
            B1 = {'x': edgerB['x'] + Boffsetx, 'y': edgerB['y'] + Boffsety}
            B2 = {'x': edgerBNext['x'] + Boffsetx, 'y': edgerBNext['y'] + Boffsety}

            if almostEqualPts(A1, A2) or almostEqualPts(B1, B2):
                continue # ignore extremely small lines

            d = segmentDistance(A1, A2, B1, B2, dir)

            if d is not None and (distance is None or d < distance):
                if not ignoreNegative or d > 0 or almostEqual(d, 0):
                    distance = d

    return distance



def to_js_array(ply):
    return [
        {'x': x, 'y':y, 'marked': False} for (x,y) in
        np.array(ply.exterior.coords.xy).T
    ]


A_poly = q
B_poly = q
inside = False
searchEdges = False
# given a static polygon A and a movable polygon B, compute a no fit polygon by
# orbiting B about A
# if the inside flag is set, B is orbited inside of A rather than outside
# if the searchEdges flag is set, all edges of A are explored for NFPs -
#  multiple
# def noFitPolygon(A, B, inside, searchEdges)

A_offsetx = 0
A_offsety = 0


A = to_js_array(A_poly)
B = to_js_array(B_poly)
assert len(A) > 2
assert len(B) > 2
minAindex = np.argmin([dct['y'] for dct in A])
maxBindex = np.argmax([dct['y'] for dct in B])


if not inside:
    # shift B such that the bottom-most point of B is at the top-most point of A. This guarantees an initial placement with no intersections
    startpoint = {
        'x': A[minAindex]['x']-B[maxBindex]['x'],
        'y': A[minAindex]['y']-B[maxBindex]['y']
    }
else:
    startpoint = searchStartPoint(A,B,B_dct,True)

NFPlist = []

A_dct = {'offsetx':0.0, 'offsety':0.0}
B_dct = {'offsetx':0.0, 'offsety':0.0}

while startpoint is not None:

    B_dct['offsetx'] = startpoint['x']
    B_dct['offsety'] = startpoint['y']

    # maintain a list of touching points/edges
    # touching

    prevvector = None # keep track of previous vector
    NFP = [{
            'x': B[0]['x']+B_dct['offsetx'],
            'y': B[0]['y']+B_dct['offsety']
        }]

    referencex = B[0]['x']+B_dct['offsetx']
    referencey = B[0]['y']+B_dct['offsety']
    startx = referencex
    starty = referencey
    counter = 0

    # sanity check, prevent infinite loop
    while counter < 10*(len(A) + len(B)):

        touching = []
        # find touching vertices/edges
        for i in range(len(A)):
            nexti = 0 if i==len(A)-1 else i+1
            for j in range(len(B)):
                nextj = 0 if j==len(B)-1 else j+1

                if (
                    almostEqual(A[i]['x'], B[j]['x']+B_dct['offsetx']) and \
                    almostEqual(A[i]['y'], B[j]['y']+B_dct['offsety'])
                ):
                    touching.append({'type': 0, 'A': i, 'B': j })
                elif (
                    onSegment(
                        A[i],
                        A[nexti],
                        {'x': B[j]['x']+B_dct['offsetx'], 'y': B[j]['y'] + B_dct['offsety']}
                    )
                ):
                    touching.append({'type': 1, 'A': nexti, 'B': j })

                elif(
                    onSegment(
                        {'x': B[j]['x']+B_dct['offsetx'], 'y': B[j]['y'] + B_dct['offsety']},
                        {'x': B[nextj]['x']+B_dct['offsetx'], 'y': B[nextj]['y'] + B_dct['offsety']},
                        A[i]
                    )
                ):
                    touching.append({'type': 2, 'A': i, 'B': nextj })



        vectors = []
        # generate translation vectors from touching vertices/edges
        for touched in touching:

            vertexA = A[touched['A']]
            vertexA['marked'] = True

            # adjacent A vertices
            prevAindex = touched['A']-1
            nextAindex = touched['A']+1

            prevAindex = len(A)-1 if prevAindex < 0 else prevAindex
            nextAindex = 0 if nextAindex >= len(A) else nextAindex

            prevA = A[prevAindex]
            nextA = A[nextAindex]

            # adjacent B vertices
            vertexB = B[touched['B']]

            prevBindex = touched['B']-1
            nextBindex = touched['B']+1

            prevBindex = len(B)-1 if prevBindex < 0 else prevBindex
            nextBindex = 0 if nextBindex >= len(B) else nextBindex

            prevB = B[prevBindex]
            nextB = B[nextBindex]

            if touched['type'] == 0:

                vA1 = {
                    'x': prevA['x']-vertexA['x'],
                    'y': prevA['y']-vertexA['y'],
                    'start': vertexA,
                    'end': prevA
                }

                vA2 = {
                    'x': nextA['x']-vertexA['x'],
                    'y': nextA['y']-vertexA['y'],
                    'start': vertexA,
                    'end': nextA
                }

                # // B vectors need to be inverted
                vB1 = {
                    'x': vertexB['x']-prevB['x'],
                    'y': vertexB['y']-prevB['y'],
                    'start': prevB,
                    'end': vertexB
                }

                vB2 = {
                    'x': vertexB['x']-nextB['x'],
                    'y': vertexB['y']-nextB['y'],
                    'start': nextB,
                    'end': vertexB
                }

                vectors.append(vA1)
                vectors.append(vA2)
                vectors.append(vB1)
                vectors.append(vB2)
            elif touched['type'] == 1:
                vectors.append({
                    'x': vertexA['x']-(vertexB['x']+B_dct['offsetx']),
                    'y': vertexA['y']-(vertexB['y']+B_dct['offsety']),
                    'start': prevA,
                    'end': vertexA
                })

                vectors.append({
                    'x': prevA['x']-(vertexB['x']+B_dct['offsetx']),
                    'y': prevA['y']-(vertexB['y']+B_dct['offsety']),
                    'start': vertexA,
                    'end': prevA
                })
            elif touched['type'] == 2:
                vectors.append({
                    'x': vertexA['x']-(vertexB['x']+B_dct['offsetx']),
                    'y': vertexA['y']-(vertexB['y']+B_dct['offsety']),
                    'start': prevB,
                    'end': vertexB
                })

                vectors.append({
                    'x': vertexA['x']-(prevB['x']+B_dct['offsetx']),
                    'y': vertexA['y']-(prevB['y']+B_dct['offsety']),
                    'start': vertexB,
                    'end': prevB
                })


        # TODO: there should be a faster way to reject vectors that will cause
        # immediate intersection. For now just check them all

        translate = None
        maxd = 0
        prevvector = None

        for vector in vectors:
            if vector['x'] == 0 and vector['y'] == 0:
                continue

            # if this vector points us back to where we came from, ignore it.
            # ie cross product = 0, dot product < 0
            if prevvector is not None and \
                vector['y'] * prevvector['y'] + vector['x'] * prevvector['x'] < 0:

                # compare magnitude with unit vectors
                vectorlength = math.sqrt(vector['x']*vector['x']+vector['y']*vector['y'])
                unitv = {'x': vector['x']/vectorlength, 'y':vector['y']/vectorlength}

                prevlength = math.sqrt(prevvector['x']*prevvector['x']+prevvector['y']*prevvector['y'])
                prevunit = {'x': prevvector['x']/prevlength, 'y':prevvector['y']/prevlength}

                # // we need to scale down to unit vectors to normalize vector length. Could also just do a tan here
                if(abs(unitv['y'] * prevunit.x - unitv.x * prevunit.y) < 0.0001):
                    continue

            d = polygonSlideDistance(A, B, A_dct, B_dct, vector, True)

            vecd2 = vector['x']*vector['x'] + vector['y']*vector['y']

            if d is None or d*d > vecd2:
                vecd = math.sqrt(vector['x']*vector['x'] + vector['y']*vector['y'])
                d = vecd

            if d is not None and d > maxd:
                maxd = d
                translate = vector



        if translate is None or almostEqual(maxd, 0):
            # // didn't close the loop, something went wrong here
            raise Exception("didn't close the loop, something went wrong here")

        translate['start']['marked'] = True
        translate['end']['marked'] = True

        prevvector = translate

        # // trim
        vlength2 = translate['x']*translate['x'] + translate['y']*translate['y']
        if (maxd*maxd < vlength2) and  not almostEqual(maxd*maxd, vlength2):
            scale = math.sqrt(maxd*maxd/vlength2)
            translate['x'] *= scale
            translate['y'] *= scale

        referencex += translate['x']
        referencey += translate['y']

        if almostEqual(referencex, startx) and almostEqual(referencey, starty):
            # // we've made a full loop
            break

        # // if A and B start on a touching horizontal line, the end point may not be the start point
        looped = False
        if len(NFP) > 0:
            for NFPi in NFP:
                if almostEqual(referencex, NFPi['x']) and \
                    almostEqual(referencey, NFPi['y']):
                    looped = True

        if looped:
            # // we've made a full loop
            break

        NFP.append({
            'x': referencex,
            'y': referencey,
        })

        B_dct['offsetx'] += translate['x']
        B_dct['offsety'] += translate['y']

        counter += 1


    if len(NFP) > 0:
        NFPlist.append(NFP)

    if not searchEdges:
        # // only get outer NFP or first inner NFP
        break

    startpoint = searchStartPoint(A,B,B_dct,inside,NFPlist)

# return NFPlist

0
